# Initializers Admin UI
# Overview
This application provides user a responsive web application, compatible across Web, tablet and mobile, to manage their store-fronts.
Below are the list of supported features
1. Ability to manage Item Catalog
    <div style="width:60px ; height:60px">
    ![Home Page](/Documentation/Image/HomePage.png?raw=true "Home Page")
    </div>
    * Supports three levels of hierarchy (Catagory, Sub-Catagory, Item)
2. Ability to Customize homepage, order workflow, third party integrations like payment gateway
    ![Configuration](/Documentation/Image/ConfigHomePage.png?raw=true "Configuration")
3. Manage Orders from an unified dashboard
    ![User Orders](/Documentation/Image/UserOrders.png?raw=true "User Orders")
4. Supports multiple themes
    ![Dark Theme](/Documentation/Image/ItemDetailsDark.png?raw=true "Dark Theme")
    ![Light Theme](/Documentation/Image/ItemSubCatagoryLight.png?raw=true "Light Theme")

# Built On
1. [ReactJs](https://reactjs.org/)
2. [Luigi](https://luigi-project.io/about)
3. [UI5 for React](https://sap.github.io/ui5-webcomponents/)

# IAM
Application has inbuilt Identity access management 
1. Creation of new storefront with admin user
2. Supports creation of multiple users under same store-front with customizable roles
    ![Register](/Documentation/Image/Register.png?raw=true "Register")

# Configuration/Customization
Application supports Configuration/Customization at 3 different levels
1. Homepage customization
2. Order workflow configuration
3. Third party integration
    ![Customization](/Documentation/Image/ConfigHomePage.png?raw=true "Customization")
    ![Customization](/Documentation/Image/ConfigHomePagePickSubCat.png?raw=true "Customization")

# Generating Mobile application
Application provides wizard for admins to automate creation of mobile apps with customizable themes
    ![GenerateApp](/Documentation/Image/GenerateApp.png?raw=true "GenerateApp")

# Adding new page with annotation
* Unified layout in list and object page is based on annotation defined [here](/src/annotation/annotation.json)
* Application generates layout(Filters, table column, etc) of the webpage at runtime based on the annotations
* Adding new page is as simple as introducing new entity in annotation
* Documentation related to annotation can be found [here](/Documentation/Annotation.md).
* In future, to make application more dynamic we can serve annotation from server.

# Links
1. [Initializers API Service](https://gitlab.com/initializers1/initializers_api)
2. [Initializers Mobile application](https://gitlab.com/initializers1/Initializer_Mobile_Application)

