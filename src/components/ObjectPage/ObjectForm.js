import React, { useState, useRef, useEffect } from "react";
import { DynamicPageHeader, ObjectPage } from "@ui5/webcomponents-react";
import LuigiClient, {
  addInitListener,
  addContextUpdateListener,
  removeContextUpdateListener,
  removeInitListener,
} from "@luigi-project/client";
import {
  Title,
  TitleLevel,
  Button,
  ButtonDesign,
  ObjectPageMode,
  ObjectPageSection,
  Avatar,
  AvatarShape,
  AvatarSize,
  FlexBox,
  Label,
  DynamicPageTitle,
} from "@ui5/webcomponents-react";

import { spacing } from "@ui5/webcomponents-react-base";
import "../../style/sap-icons.css";

import CustomHeaderContent from "./header/CustomHeaderContent.js";
import CustomKeyInfo from "./header/CustomKeyInfo.js";
import CustomFacet from "../ObjectPage/Facet/CustomFacet.js";
import annotation from "../../annotation/annotation.json";

import getFieldValue from "../../functions/getFieldValue.js";
import axios from "../../api/axios";
import "@ui5/webcomponents-icons/dist/decline.js";
import "@ui5/webcomponents-icons/dist/upload.js";
import "@ui5/webcomponents-icons/dist/navigation-left-arrow.js";

const ObjectForm = () => {
  const [formType, setFormType] = useState("");
  const [formAction, setFormAction] = useState("");
  const [pathValue, setPathValue] = useState("");
  // for associations we use this parent ID 
  const [parentId, setParentId] = useState("");
  const [previousOperation, setPreviousOperation] = useState("");
  const [EditSaveButton, setEditSaveButton] = useState("Edit");
  let isDirty = false;
  const [header, setHeader] = useState({});
  const [property, setProperty] = useState({});
  const [suggestion, setSuggestion] = useState({});
  const [entity, setEntity] = useState({});
  const [facets, setFacets] = useState([]);
  const [defaultAction, setDefaultAction] = useState({});
  const [formData, setFormData] = useState({});

  const dataLoadIssueMessage = {
    text: "unable to load Data, object check your internet connection once",
    type: "error",
    closeAfter: 3000,
  };

  useEffect(() => {
    if (
      pathValue !== "" &&
      pathValue !== "id" &&
      formType !== "" &&
      formAction &&
      previousOperation !== "Create"
    ) {
      axios
        .get(`${formType}/${pathValue}`)
        .then((data) => {
          setEntity(data.data);
        })
        .catch((err) => {
          LuigiClient.uxManager().showAlert(dataLoadIssueMessage);
        });
    }
    if (formAction === "Read") {
      setHeader(annotation[formType].header);
      setEditSaveButton("Edit");
      setDefaultAction(annotation[formType].action);
      setProperty(annotation[formType].property);
      setSuggestion(annotation[formType].suggestion);
      setFormData(annotation[formType].metadata);
      setFacets(
        annotation[formType].facet === undefined
          ? []
          : annotation[formType].facet
      );
      setPreviousOperation("Read");
    } else if (formAction === "Create") {
      setDefaultAction(annotation[formType].action);
      setEditSaveButton("Save");
      setProperty(annotation[formType].property);
      setSuggestion(annotation[formType].suggestion);
      setEntity(annotation[formType].metadata);
      setFacets(
        annotation[formType].facet === undefined
          ? []
          : getFacetWithoutAssociation(annotation[formType].facet)
      );
      setFormData(JSON.parse(JSON.stringify(annotation[formType].metadata)));
    }
  }, [formAction, formType, pathValue]);

  const getFacetWithoutAssociation = (facets) => {
    return facets.filter((facet) => facet.type === "fieldGroup");
  };
  useEffect(() => {
    const initListener = addInitListener((e) => {
      setFormType(LuigiClient.getNodeParams().type);
      setPathValue(LuigiClient.getPathParams().id);
      setFormAction(LuigiClient.getNodeParams().action);
      setParentId(LuigiClient.getNodeParams().parentID);
      LuigiClient.uxManager().hideLoadingIndicator();
    });
    const updateListener = addContextUpdateListener((e) => {
      if (pathValue !== LuigiClient.getPathParams().id) {
        setPathValue(LuigiClient.getPathParams().id);
      }
      if (formType !== LuigiClient.getNodeParams().type) {
        setFormType(LuigiClient.getNodeParams().type);
      }
      if (formAction !== LuigiClient.getNodeParams().action) {
        setFormAction(LuigiClient.getNodeParams().action);
      }
      if (parentId !== LuigiClient.getNodeParams().parentId) {
        setParentId(LuigiClient.getNodeParams().parentId);
      }
    });
    return function cleanUp() {
      removeInitListener(initListener);
      removeContextUpdateListener(updateListener);
    };
  }, []);

  const ImageUploadpopoverRef = useRef();

  const setParentValue = (property, data, value, type, entity) => {
    Object.keys(property).forEach((key) => {
      if (property[key].parentId === "true") {
        if (type === "Edit") {
          data[key] = entity[key];
        } else {
          data[key] = value;
        }
      }
    });
  };

  const onEditAction = () => {
    if (EditSaveButton === "Edit") {
      setFormData(JSON.parse(JSON.stringify(annotation[formType].metadata)));
      setEditSaveButton("Save");
    } else {
      if (isDirty) {
        let method = "";
        LuigiClient.uxManager().showLoadingIndicator();
        //check HTTP method type(id contains number => Edit)
        if (pathValue === "id") {
          //create
          method = "post";
          //check if parentid exist
          if (parentId !== "" || parentId !== undefined) {
            setParentValue(property, formData, parentId);
          }
        } else {
          //edit
          method = "put";
          formData["id"] = entity["id"];
          setParentValue(property, formData, parentId, "Edit", entity);
        }
        let bodyFormData = new FormData();
        bodyFormData.append(formType, JSON.stringify(formData));
        axios({
          method: method,
          url: `${formType}`,
          data: formData,
        })
          .then(function (response) {
            setEntity(response.data);
            setPathValue(response.data["id"]);
            setFormAction("Read");
            isDirty = false;
            LuigiClient.uxManager().hideLoadingIndicator();
          })
          .catch(function (response) {
            const errorMessage = {
              text:
                response.response === undefined
                  ? response.message
                  : response.response.data.message,
              type: "error",
              closeAfter: 5000,
            };
            LuigiClient.uxManager().hideLoadingIndicator();
            LuigiClient.uxManager().showAlert(errorMessage);
          });
      }
      setEditSaveButton("Edit");
    }
  };

  const onDeleteAction = () => {
    axios({
      method: "delete",
      url: `${formType}/${pathValue}`,
    })
      .then(function (response) {
        LuigiClient.uxManager().hideLoadingIndicator();
        LuigiClient.linkManager().goBack();
      })
      .catch(function (response) {
        const errorMessage = {
          text:
            response.response === undefined
              ? response.message
              : response.response.data.message,
          type: "error",
          closeAfter: 3000,
        };
        LuigiClient.uxManager().hideLoadingIndicator();
        LuigiClient.uxManager().showAlert(errorMessage);
      });
  };
  const onInputChange = (e) => {
    const parentID = e.target.id;
    const temp = parentID.split(".");
    if (temp.length > 1) {
      if (formData[temp[0]] === null) {
        formData[temp[0]] = {};
        formData[temp[0]][temp[1]] = getFieldValue(e);
      } else {
        formData[temp[0]][temp[1]] = getFieldValue(e);
      }
    } else {
      formData[parentID] = getFieldValue(e);
    }
    isDirty = true;
  };

  const settings = {
    header: "Warning",
    body: "you haven't saved data!! Are you sure you want to exit",
    buttonConfirm: "Yes",
    buttonDismiss: "No",
  };
  const resetValue = () => {
    setProperty({});
    setEntity({});
    setHeader({});
    setFacets([]);
    setDefaultAction({});
    setFormData({});
    setFormAction("");
    setFormType("");
    setPathValue("");
  };

  const onGoBack = () => {
    if (isDirty) {
      LuigiClient.uxManager()
        .showConfirmationModal(settings)
        .then(() => {
          LuigiClient.linkManager().goBack();
        });
    } else {
      resetValue();
      LuigiClient.linkManager().goBack();
    }
  };
  if (!Object.keys(property) || !Object.keys(entity).length) {
    return (
      <>
        <main
          className="fd-page fd-page--transparent"
          style={{
            width: "100%",
            height: "100%",
            margin: "0",
            overflow: "hidden",
          }}
        >
          <header>
            <div class="fd-bar__left">
              <div
                class="fd-bar__element"
                style={{ ...spacing.sapUiSmallMarginBegin }}
              >
                <button
                  class="fd-button fd-button--transparent sap-icon--navigation-left-arrow"
                  onClick={onGoBack}
                ></button>
              </div>
              <div class="fd-bar__element">
                <Title level={TitleLevel.H3}>{formType}</Title>
              </div>
            </div>
          </header>
        </main>
      </>
    );
  } else
    return (
      <main
        className="fd-page fd-page--transparent"
        style={{
          width: "100%",
          height: "100%",
          margin: "0",
          overflow: "hidden",
        }}
      >
        <header>
          <div class="fd-bar__left">
            <div
              class="fd-bar__element"
              style={{ ...spacing.sapUiSmallMarginBegin }}
            >
              <ui5-button
                icon="navigation-left-arrow"
                design="Transparent"
                accessible-name-ref="lblAway"
                onClick={onGoBack}
              ></ui5-button>
            </div>
            <div class="fd-bar__element">
              <Title level={TitleLevel.H3}>{formType}</Title>
            </div>
          </div>
        </header>
        <div class="fd-page__content" style={{ width: "100%" }}>
          <ObjectPage
            headerContent={
              header === undefined ? (
                <></>
              ) : (
                <DynamicPageHeader>
                  <FlexBox alignItems="Center" wrap="Wrap">
                    <CustomHeaderContent
                      headerContent={header.headerContent}
                      property={property}
                      entity={entity}
                    />
                  </FlexBox>
                </DynamicPageHeader>
              )
            }
            headerTitle={
              <DynamicPageTitle
                actions={[
                  defaultAction === undefined ||
                  defaultAction.deleteEnabled !== "true" ? (
                    <Label hidden={true} id="emptyLabelobjectpage" />
                  ) : (
                    <Button
                      id="b2"
                      class="fd-button fd-button--emphasized fd-button--compact"
                      key="save-action-default-action"
                      design={ButtonDesign.Negative}
                      onClick={onDeleteAction}
                    >
                      Delete
                    </Button>
                  ),
                  defaultAction === undefined ||
                  defaultAction.updateEnabled !== "true" ? (
                    <Label hidden={true} id="emptyLabelobjectpage2" />
                  ) : (
                    <Button
                      id="b2"
                      class="fd-button fd-button--emphasized fd-button--compact"
                      key="edit-action-default-action"
                      design={ButtonDesign.Emphasized}
                      onClick={onEditAction}
                    >
                      {EditSaveButton}
                    </Button>
                  ),
                ]}
                header={header === undefined ? <></> : entity[header.title]}
                showSubHeaderRight
                subHeader={
                  header === undefined ? <></> : entity[header.subTitle]
                }
              >
                {header === undefined ? (
                  <></>
                ) : (
                  <CustomKeyInfo
                    headerContent={header.keyInfos}
                    property={property}
                    data={entity}
                  />
                )}
              </DynamicPageTitle>
            }
            image={
              header === undefined ? (
                <></>
              ) : header.image === undefined ? (
                <></>
              ) : (
                <Avatar
                  initials={"i"}
                  shape={AvatarShape.Square}
                  size={AvatarSize.XL}
                >
                  {entity[header.image] ? (
                    <img
                      alt=""
                      src={entity[header.image]}
                      style={{ objectFit: "contain" }}
                    ></img>
                  ) : (
                    <></>
                  )}
                </Avatar>
              )
            }
            mode={ObjectPageMode.Default}
            showHideHeaderButton={true}
            headerContentPinnable={true}
          >
            {facets.map((facet) => (
              <ObjectPageSection
                titleText={facet.label === undefined ? "" : facet.label}
                id={facet.id === undefined ? "" : facet.id}
              >
                <CustomFacet
                  facet={facet}
                  property={property}
                  context={formType}
                  imageUploadpopoverRef={ImageUploadpopoverRef}
                  editStatus={EditSaveButton}
                  entity={entity}
                  suggestion={suggestion}
                  onInputChange={onInputChange}
                />
              </ObjectPageSection>
            ))}
          </ObjectPage>
        </div>
      </main>
    );
};

export default ObjectForm;
