import React, { useState } from "react";
import {
  Button,
  ButtonDesign,
  Title,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxDirection,
  FlexBoxJustifyContent,
  FlexBoxWrap,
  Loader,
  FileUploader,
  UploadCollection,
  UploadCollectionItem,
  ListMode,
} from "@ui5/webcomponents-react";
import axios from "../../../api/axios";
import { spacing } from "@ui5/webcomponents-react-base";
import LuigiClient from "@luigi-project/client";
export default function CustomImageHolder(props) {
  const { parentId, ImageUploadpopoverRef, entity, context } = props;
  const [enableBusyIndicator, setEnableBusyIndicator] = useState(false);
  const multiple =
    props.property === undefined
      ? false
      : props.property.multiple === undefined
      ? false
      : props.property.multiple;
  const [images, setImages] = useState(multiple ? entity : entity? [entity]: []);
  const onImageUpload = (event) => {
    if (event.target.files) {
      setEnableBusyIndicator(true);
      let webWorker = new Worker("./UploadFile.js");
      webWorker.postMessage({
        msg: "uploadImage",
        file: event.target.files,
        type: context,
        parentId,
        domain: process.env.REACT_APP_DOMAIN,
      });
      webWorker.onmessage = (event) => {
        if (
          event &&
          event.data &&
          event.data.newImages &&
          event.data.msg === "success"
        ) {
          if (multiple) {
            setImages([...images, ...event.data.newImages]);
          } else {
            setImages([...event.data.newImages]);
          }
          setEnableBusyIndicator(false);
          LuigiClient.uxManager().hideLoadingIndicator();
        } else if (event && event.data && event.data.msg === "Error") {
          const error = event.data.errorMessage;
          const errorMessage = {
            text:
              error.response === undefined
                ? error
                : error.response.data.message,
            type: "error",
            closeAfter: 8000,
          };
          setEnableBusyIndicator(false);
          LuigiClient.uxManager().showAlert(errorMessage);
        }
      };
      webWorker.onerror = (error) => {
        const errorMessage = {
          text: error.response,
          type: "error",
          closeAfter: 8000,
        };
        setEnableBusyIndicator(false);
        LuigiClient.uxManager().showAlert(errorMessage);
      };
    }
  };
  const onImageDelete = (event) => {
    let form = new FormData();
    form.append("type", context);
    //get id of current entity
    form.append("id", parentId);
    form.append("images", [event.detail.item.id]);
    var filteredImage = images.filter(
      (image) => image !== event.detail.item.id
    );
    setImages(filteredImage);
    axios({
      method: "put",
      url: `/imageUpload`,
      data: form,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
      .then((response) => {})
      .catch((response) => {
        const errorMessage = {
          text:
            response.response.data === undefined
              ? response.message
              : response.response.data.message,
          type: "error",
          closeAfter: 3000,
        };
        LuigiClient.uxManager().showAlert(errorMessage);
      });
  };
  if (enableBusyIndicator) {
    return (
      <div>
        <FlexBox
          alignItems={FlexBoxAlignItems.Center}
          direction={FlexBoxDirection.Row}
          justifyContent={FlexBoxJustifyContent.SpaceAround}
          wrap={FlexBoxWrap.Wrap}
        >
          <Loader progress="60%" />
          <Title>Uploading Images, Please Wait...</Title>
        </FlexBox>
      </div>
    );
  } else {
    return (
      <div>
        <FileUploader
          id="fileUploder-id"
          accept="image"
          multiple={multiple}
          placeholder={"click to upload"}
          onChange={onImageUpload}
          hideInput={true}
        >
          <Button
            id="openPopoverButton"
            design={ButtonDesign.Emphasized}
            style={{
              ...spacing.sapUiForceWidthAuto,
              ...spacing.sapUiSmallMarginBottom,
            }}
            icon="upload"
            onClick={(e) => {
              ImageUploadpopoverRef.current.showAt(e.target);
            }}
          >
            Upload File
          </Button>
        </FileUploader>
        <UploadCollection
          mode={ListMode.Delete}
          onItemDelete={onImageDelete}
          onDrop={(e) => {
            console.log(e);
          }}
        >
          {images && images.map((image) => (
            <UploadCollectionItem
              id={image}
              fileName="Click here to view"
              fileNameClickable={true}
              mode="None"
              uploadState="Complete"
              type="Inactive"
              onFileNameClick={(e) => {
                window.open(e.target.id, "_blank");
              }}
            >
              <img
                alt={"item details"}
                src={image}
                style={{ objectFit: "contain" }}
                slot="thumbnail"
              ></img>
            </UploadCollectionItem>
          ))}
        </UploadCollection>
      </div>
    );
  }
}
