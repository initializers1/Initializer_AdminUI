import React from "react";

import {
  Label,
  Text,
  FlexBox,
  FlexBoxJustifyContent,
  FlexBoxDirection,
  FlexBoxWrap,
  FlexBoxAlignItems,
} from "@ui5/webcomponents-react";
import { spacing } from "@ui5/webcomponents-react-base";
import ItemList from "../../ListPage/item-list.js";
import CustomImageHolder from "./CustomImageHolder.js";
import CustomFieldItem from "../../CustomFieldItem.js";

export default function CustomFacet(props) {
  const { context } = props;
  const fields = props.facet.field;
  const editStatus = props.editStatus;
  const association = props.facet.association;
  const type = props.facet.type;
  const parentId =
    Object.keys(props.entity).length <= 0 ? "" : props.entity["id"];
  const property =
    association === undefined ? props.property : props.property[association];
  const entity =
    props.entity === undefined || Object.keys(props.entity).length <= 0
      ? []
      : association === undefined
      ? props.entity
      : props.entity[association];

  const suggestion = props.suggestion;
  const imageUploadpopoverRef = props.imageUploadpopoverRef;
  if (property === undefined || Object.keys(property).length < 1) {
    return <></>;
  } else {
    switch (type) {
      case "fieldGroup":
        return (
          <FlexBox
            direction={FlexBoxDirection.Row}
            justifyContent={FlexBoxJustifyContent.SpaceAround}
            wrap={FlexBoxWrap.Wrap}
            alignItems={FlexBoxAlignItems.Center}
          >
            {fields.map((field) => (
              <div style={{ ...spacing.sapUiContentPadding }}>
                <Label>{property[field].label}</Label>
                <div>
                  {editStatus === "Edit" ? (
                    <Text>
                      {entity === null || entity === undefined
                        ? ""
                        : entity[
                            property[field].textAssociation === undefined
                              ? field
                              : property[field].textAssociation
                          ]}
                    </Text>
                  ) : property[field].readOnly === undefined ? (
                    <CustomFieldItem
                      label={
                        property[field].label === undefined
                          ? field
                          : property[field].label
                      }
                      component={property[field].editComponent}
                      suggestion={
                        suggestion === undefined
                          ? {}
                          : props.suggestion[property[field].suggestion]
                      }
                      field={
                        association === undefined
                          ? field
                          : `${association}.${field}`
                      }
                      value={
                        entity === null || entity === undefined
                          ? ""
                          : entity[
                              property[field].textAssociation === undefined
                                ? field
                                : property[field].textAssociation
                            ]
                      }
                      onFilterChange={props.onInputChange}
                    />
                  ) : (
                    <>
                      <div>
                        <Label>
                          {property[field].label === undefined
                            ? field
                            : property[field].label}
                        </Label>
                      </div>
                      <Text label={"a"}>
                        {entity === null || entity === undefined
                          ? ""
                          : entity[
                              property[field].textAssociation === undefined
                                ? field
                                : property[field].textAssociation
                            ]}
                      </Text>
                    </>
                  )}
                </div>
              </div>
            ))}
          </FlexBox>
        );
      case "lineItem":
        const data = {};
        data["property"] = property;
        data["data"] = entity;
        return (
          <ItemList
            parentId={parentId}
            data={entity === undefined ? [] : entity}
            context={props.facet.contextType}
            action={props.facet.action}
            isDerived={true}
            property={property}
            listPage={props.facet.listPage}
          />
        );
      case "imageLinks":
        return (
          <CustomImageHolder
            parentId={parentId}
            context={context}
            entity={entity}
            editStatus={editStatus}
            field={association}
            property={property}
            ImageUploadpopoverRef={imageUploadpopoverRef}
          />
        );
      default:
        return <></>;
    }
  }
}
