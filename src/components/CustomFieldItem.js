import React, { useEffect, useState } from "react";
import {
  MultiComboBox,
  MultiComboBoxItem,
  Input,
  SuggestionItem,
  DatePicker,
} from "@ui5/webcomponents-react";
import "@ui5/webcomponents/dist/features/InputSuggestions.js";
import "@ui5/webcomponents/dist/DateRangePicker";
import axios from "../api/axios";

// {
//   /*input
// component
// field
// value
// suggestion
// onFilterChange
// */
// }
export default function CustomFieldItem(props) {
  const component = props.component;
  const [suggestion, setSuggestion] = useState([]);
  const field = props.field;
  const value = props.value === undefined ? "" : props.value;
  useEffect(() => {
    if (
      props.suggestion !== undefined &&
      props.suggestion.valueType === "constant"
    ) {
      if (Array.isArray(props.suggestion.value)) {
        setSuggestion(props.suggestion.value);
      }
    } else if (
      props.suggestion !== undefined &&
      props.suggestion.valueType === "standard"
    ) {
      axios.get(`${props.suggestion.value}`).then((response) => {
        setSuggestion(response.data ? response.data : []);
      });
    }
  }, [props.field, props.suggestion]);
  switch (component) {
    case "MultiComboBox":
      if (Array.isArray(suggestion)) {
        return (
          <MultiComboBox
            allowCustomValues={true}
            id={field}
            key={field}
            value={value}
            onSelectionChange={props.onFilterChange}
            onKeyPress={props.onFilterChange}
          >
            {suggestion.map((suggestion) => (
              <MultiComboBoxItem
                key={suggestion.key}
                id={suggestion.key}
                text={suggestion.value}
              />
            ))}
          </MultiComboBox>
        );
      } else {
        return <></>;
      }
    case "ComboBox":
      var suggestionKey = 0; 
      if (Array.isArray(suggestion)) {
        return (
          <Input
            id={field}
            value={value}
            showSuggestions
            onInput={props.onFilterChange}
            onKeyPress={props.onFilterChange}
          >
            {suggestion.map((suggestion) => (
              <SuggestionItem
                key={`SuggestionItem--${suggestionKey++}`}
                text={suggestion.value}
                itemID={suggestion.key}
              />
            ))}
          </Input>
        );
      } else {
        return <></>;
      }
    case "Input":
      return (
        <Input
          id={field}
          showSuggestions={false}
          value={value}
          nodeName="UI5-COMBOBOX"
          style={{ width: "100%" }}
          onChange={props.onFilterChange}
          onKeyPress={props.onFilterChange}
        ></Input>
      );
    case "DateRange":
      return <ui5-daterange-picker id={field}></ui5-daterange-picker>;
    case "DatePicker":
      return (
        <DatePicker
          id={field}
          primaryCalendarType="Gregorian"
          onChange={props.onFilterChange}
          onKeyPress={props.onFilterChange}
        />
      );
    default:
      return <></>;
  }
}
