import React, { useEffect, useRef, useState } from "react";
import LuigiClient from "@luigi-project/client";
import {
  Bar,
  Wizard,
  WizardStep,
  Title,
  Button,
  ButtonDesign,
  FlexBox,
  Carousel,
  FlexBoxDirection,
  Page,
  MessageStrip,
  Toast,
} from "@ui5/webcomponents-react";
import axios from "../../api/axios";
import GeneralInformation from "./GeneralInformation";
import ThemeInformation from "./ThemeInformation";
import TermsInformation from "./TermsInformation";
import "@ui5/webcomponents-icons/dist/key-user-settings.js";
import "@ui5/webcomponents-icons/dist/image-viewer.js";
import "@ui5/webcomponents-icons/dist/complete.js";
import "@ui5/webcomponents-icons/dist/ipad.js";
import "@ui5/webcomponents-icons/dist/iphone.js";
import "@ui5/webcomponents-icons/dist/laptop.js";
import "@ui5/webcomponents-icons/dist/touch";
import "@ui5/webcomponents-icons/dist/master-task-triangle-2";
import "@ui5/webcomponents-icons/dist/full-screen";
import "@ui5/webcomponents-icons/dist/exit-full-screen";

export default function GenerateApp() {
  const firstStep = useRef();
  const secondStep = useRef();
  const thirdStep = useRef();
  const [updatedData, setUpdatedData] = useState({});
  const [selectedTheme, setSelectedTheme] = useState("");
  const [agreeTerms, setAgreeTerms] = useState(false);
  const [icon, setIcon] = useState("");
  const [loading, setLoading] = useState(true);
  const [isFinal, setIsFinal] = useState(false);
  const [displayThemeInFullScreen, setDisplayThemeInFullScreen] =
    useState(false);
  const [imagesToDisplay, setImagesToDisplay] = useState([]);
  const [errMsgForGenInfo, setErrMsgForGenInfo] = useState({
    display: false,
    msg: "",
  });
  const [errMsgForTheme, setErrMsgForTheme] = useState({
    display: false,
    msg: "",
  });
  const [errMsgForTerms, setErrMsgForTerms] = useState(false);
  const buildToast = useRef();
  const onSelectionChange = () => {
    console.log(updatedData);
    if (firstStep.current.selected) {
      if (updatedData.appCategory && updatedData.supportedPlatform) {
        setErrMsgForGenInfo({ display: false, msg: "" });
        secondStep.current.disabled = false;
        secondStep.current.selected = true;
      } else {
        setErrMsgForGenInfo({
          display: true,
          msg: "Please provide your application category and platforms you wish to support",
        });
      }
    } else if (secondStep.current.selected) {
      setUpdatedData({ ...updatedData, selectedTheme: selectedTheme });
      setIsFinal(true);
      thirdStep.current.disabled = false;
      thirdStep.current.selected = true;
    } else {
      if (agreeTerms) {
        setErrMsgForTerms(false);
        let form = new FormData();
        const updatedStringData = JSON.stringify(updatedData);
        const updatedBlobData = new Blob([updatedStringData], {
          type: "application/json",
        });
        form.append("appDetails", updatedBlobData);
        form.append("image", icon);
        LuigiClient.uxManager().showLoadingIndicator();
        axios
          .post("/generateApp", form, {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          })
          .then((response) => {
            LuigiClient.uxManager().hideLoadingIndicator();
            buildToast.current.show();
            LuigiClient.linkManager().goBack(response?.data);
          })
          .catch((error) => {
            LuigiClient.uxManager().hideLoadingIndicator();
            LuigiClient.uxManager()
              .showAlert({
                text: error.response?.data?.message
                  ? error.response?.data?.message
                  : "Opps, there was some issue while building your app, Please try again after some time",
                type: "error",
              })
              .then(() => {
                LuigiClient.linkManager().goBack();
              });
              LuigiClient.linkManager().goBack();
          });
        //make call
      } else {
        setErrMsgForTerms(true);
      }
    }
  };
  useEffect(() => {
    setLoading(false);
  }, []);
  if (loading) {
    return <></>;
  }
  return (
    <FlexBox
      style={{
        height: `${document.body.offsetHeight}px`,
        overflowX: "clip",
      }}
      direction={FlexBoxDirection.Column}
    >
      <Wizard
        hidden={displayThemeInFullScreen ? displayThemeInFullScreen : null}
      >
        <WizardStep
          ref={firstStep}
          icon="key-user-settings"
          titleText="General Information"
          selected
          tooltip="general information about your app"
        >
          <MessageStrip
            hidden={errMsgForGenInfo.display ? null : true}
            hideCloseButton
            hideIcon
          >
            {errMsgForGenInfo.msg}
          </MessageStrip>
          <Title style={{ margin: "20px", fontWeight: "bold" }}>
            Application Details
          </Title>
          <GeneralInformation
            updatedData={updatedData}
            setUpdatedData={setUpdatedData}
            setIcon={setIcon}
          />
        </WizardStep>
        <WizardStep
          ref={secondStep}
          icon="image-viewer"
          titleText="Themes"
          disabled
          tooltip="Suitable theme for your app"
        >
          <MessageStrip hidden={errMsgForTheme.display ? null : true}>
            {errMsgForTheme.msg}
          </MessageStrip>
          <Title style={{ margin: "20px", fontWeight: "bold" }}>
            Theme Selection
          </Title>
          <div style={{ margin: "30px" }}>
            <ThemeInformation
              setDisplayThemeInFullScreen={setDisplayThemeInFullScreen}
              setImagesToDisplay={setImagesToDisplay}
              setSelectedTheme={setSelectedTheme}
            />
          </div>
        </WizardStep>
        <WizardStep ref={thirdStep} disabled icon="accept" titleText="Finish">
          <MessageStrip
            hidden={errMsgForTerms ? null : true}
            hideCloseButton
            hideIcon
          >
            {
              "You should agree to all Terms of Services to start building your Application"
            }
          </MessageStrip>
          <Title style={{ margin: "20px", fontWeight: "bold" }}>
            Terms of Services
          </Title>
          <TermsInformation setAgreeTerms={setAgreeTerms} />
        </WizardStep>
      </Wizard>
      <Bar
        hidden={displayThemeInFullScreen ? displayThemeInFullScreen : null}
        endContent={
          <Button
            hidden={displayThemeInFullScreen ? displayThemeInFullScreen : null}
            design="Emphasized"
            onClick={onSelectionChange}
          >
            {isFinal ? "Confirm" : "Next"}
          </Button>
        }
        style={{ position: "absolute", bottom: 0 }}
      ></Bar>
      <Page
        hidden={!displayThemeInFullScreen ? !displayThemeInFullScreen : null}
        header={
          <Bar
            endContent={
              <Button
                icon="exit-full-screen"
                style={{ alignSelf: "flex-end" }}
                design={ButtonDesign.Transparent}
                onClick={() => setDisplayThemeInFullScreen(false)}
              />
            }
          ></Bar>
        }
      >
        <Carousel
          hidden={!displayThemeInFullScreen ? !displayThemeInFullScreen : null}
          cyclic={true}
        >
          {imagesToDisplay?.map((image, index) => (
            <img
              key={"theme--fullscreen--" + index}
              style={{ objectFit: "contain", height: "100%" }}
              src={image}
            ></img>
          ))}
        </Carousel>
      </Page>
      <Toast ref={buildToast}>Build Started</Toast>
    </FlexBox>
  );
}
