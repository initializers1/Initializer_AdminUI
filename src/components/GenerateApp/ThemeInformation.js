import React, { useEffect, useState } from "react";
import {
  Card,
  Button,
  ButtonDesign,
  FlexBox,
  ResponsiveGridLayout,
  Carousel,
  RadioButton,
  FlexBoxJustifyContent,
  FlexBoxAlignItems,
  Label,
} from "@ui5/webcomponents-react";
import axios from "../../api/axios";

export default function ThemeInformation(props) {
  const {
    setDisplayThemeInFullScreen,
    setImagesToDisplay,
    setSelectedTheme
  } = props;
  const [themes, setThemes] = useState([]);
  const onThemeSelect = (event) => {
    setSelectedTheme(event.target.id);
  };
  useEffect(() => {
    axios
      .get("/themes")
      .then((response) => {
        setThemes(response?.data);
        setSelectedTheme(response?.data[0]?.id);
      })
      .catch(() => {});
  }, []);
  return (
    <ResponsiveGridLayout>
      {themes.map((theme, index) => (
        <div
          key={"theme--container--" + theme?.id}
          style={{
            gridColumn: "span 3",
          }}
        >
          <div>
            <Card style={{ height: "440px", width: "180px", margin: "10px" }}>
              <FlexBox
                direction={"Row"}
                justifyContent={FlexBoxJustifyContent.SpaceBetween}
                alignItems={FlexBoxAlignItems.Center}
              >
                <RadioButton
                  id={theme?.id}
                  checked={index === 0 ? true : false}
                  name="selectTheme"
                  onChange={onThemeSelect}
                />
                <Label style={{ fontWeight: "bolder" }}>{theme?.name}</Label>
                <Button
                  icon="full-screen"
                  design={ButtonDesign.Transparent}
                  onClick={() => {
                    setImagesToDisplay(theme.images);
                    setDisplayThemeInFullScreen(true);
                  }}
                />
              </FlexBox>
              <Carousel
                style={{
                  height: "400px",
                  width: "180px",
                  minWidth: "10px",
                }}
                cyclic={true}
              >
                {theme.images?.map((image, index) => (
                  <img
                    key={"theme--image--" + index}
                    style={{
                      width: "100%",
                      height: "100%",
                      objectFit: "contain",
                    }}
                    src={image}
                  ></img>
                ))}
              </Carousel>
            </Card>
          </div>
        </div>
      ))}
    </ResponsiveGridLayout>
  );
}
