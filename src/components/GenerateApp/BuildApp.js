import {
  FlexBoxDirection,
  Text,
  Avatar,
  ComboBox,
  ComboBoxItem,
  CheckBox,
  Card,
  Label,
  Icon,
  Button,
  Input,
  Form,
  FormItem,
  FlexBox,
  ResponsiveGridLayout,
  FileUploader,
  FlexBoxJustifyContent,
  FlexBoxAlignItems,
  ObjectStatus,
  ValueState,
  Bar,
  ButtonDesign,
  Toast,
  Page,
  Link,
  MessageStrip,
  MessageStripDesign,
  IllustratedMessage,
} from "@ui5/webcomponents-react";
import React, { useEffect, useRef, useState } from "react";
import TestInformation from "./TestInformation";
import DisplaySupportedPlatform from "./DisplaySupportedPlatform";
import axios from "../../api/axios";
import LuigiClinet, {
  addInitListener,
  addContextUpdateListener,
  removeInitListener,
  removeContextUpdateListener,
} from "@luigi-project/client";
import "@ui5/webcomponents-fiori/dist/illustrations/NoActivities.js";
import "@ui5/webcomponents-fiori/dist/illustrations/UnableToLoad";
export default function BuildApp() {
  var contentLabelSize = 16;
  const buildStatusState = {
    success: ValueState.Success,
    failed: ValueState.Error,
    running: ValueState.Information,
    created: ValueState.Information,
  };
  const [appDetails, setAppDetails] = useState({});
  const [moreInformation, setMoreInformation] = useState({
    design: "",
    msg: "",
  });
  const [disableBuildButton, setDisableBuildButton] = useState(false);
  const [unableToLoadData, setUnableToLoadData] = useState(false);
  const formatTriggeredAt = (triggeredAt) => {
    if (triggeredAt) {
      var date = new Date(triggeredAt);
      if (date) {
        return date.toDateString() + " " + date.toLocaleTimeString();
      } else {
        return "";
      }
    } else {
      return "";
    }
  };
  const onCreateorRebuildMyApp = () => {
    LuigiClinet.linkManager().openAsModal("/admin-home/generateApp");
  };
  const updateAppDetails = (appDetails) => {
    setAppDetails(appDetails);
    if (!appDetails?.pipelineStatus) {
      onCreateorRebuildMyApp();
    } else if (appDetails?.pipelineStatus === "success") {
      setDisableBuildButton(false);
      setMoreInformation({
        design: MessageStripDesign.Information,
        msg: 'If you wish to change any data, click on "Rebuild my App" button and update your data',
      });
    } else if (appDetails?.pipelineStatus === "failed") {
      setDisableBuildButton(false);
      setMoreInformation({
        design: MessageStripDesign.Negative,
        msg: 'There was some error while building your App, Please click on "Rebuild my App" to start the process again',
      });
    } else {
      setDisableBuildButton(true);
      setMoreInformation({
        design: "",
        msg: "",
      });
    }
  };
  const fetchAppDetails = () => {
    LuigiClinet.uxManager().showLoadingIndicator();
    axios
      .get("appDetails")
      .then((response) => {
        updateAppDetails(response?.data);
        setUnableToLoadData(false);
        LuigiClinet.uxManager().hideLoadingIndicator();
      })
      .catch((error) => {
        setUnableToLoadData(true);
        LuigiClinet.uxManager().hideLoadingIndicator();
      });
  };
  useEffect(() => {
    var initListener = addInitListener((event) => {
      if (event.goBackContext) {
        updateAppDetails(event.goBackContext);
      } else {
        fetchAppDetails();
      }
    });
    var updateListener = addContextUpdateListener((event) => {
      fetchAppDetails();
    });
    return function cleanup() {
      removeInitListener(initListener);
      removeContextUpdateListener(updateListener);
    };
  }, []);
  return (
    <>
      <Bar
        endContent={
          <Button
            design={ButtonDesign.Emphasized}
            onClick={onCreateorRebuildMyApp}
            disabled={disableBuildButton}
          >
            {!appDetails?.pipelineStatus || appDetails?.pipelineStatus === ""
              ? "Create My App"
              : "Rebuild My App"}
          </Button>
        }
      ></Bar>
      {!appDetails?.pipelineStatus || appDetails?.pipelineStatus === "" ? (
        unableToLoadData ? (
          <IllustratedMessage name="UnableToLoad" />
        ) : (
          <IllustratedMessage
            name="NoActivities"
            titleText="You have not built your App yet"
            subtitleText='Click on "Create my App" Button and provided necessary information to start creating your App'
          />
        )
      ) : (
        <ResponsiveGridLayout>
          <div
            style={{
              gridColumn: "span 7",
              margin: "5px",
            }}
          >
            <FlexBox
              style={{
                width: "100%",
                height: "100%",
                zIndex: 1,
                margin: "10px",
              }}
              direction={FlexBoxDirection.Column}
              justifyContent={FlexBoxJustifyContent.SpaceBetween}
            >
              <FlexBox
                direction={FlexBoxDirection.Row}
                justifyContent={FlexBoxJustifyContent.Start}
                alignItems={FlexBoxAlignItems.Center}
              >
                <Avatar
                  icon="employee"
                  size="XS"
                  style={{ marginRight: "10px" }}
                  children={
                    <img src={appDetails?.appIcon} alt="app icons"></img>
                  }
                />
                <Label style={{ fontWeight: "bolder", fontSize: 22 }}>
                  {appDetails?.appName}
                </Label>
              </FlexBox>
              <Form
                style={{ marginLeft: "60px", marginTop: "40px" }}
                columnsXL={5}
              >
                <FormItem
                  label={
                    <Label
                      style={{
                        alignSelf: "center",
                        fontSize: contentLabelSize,
                      }}
                    >
                      Build Status
                    </Label>
                  }
                >
                  <ObjectStatus
                    showDefaultIcon={false}
                    state={buildStatusState[appDetails?.pipelineStatus]}
                    children={appDetails?.pipelineStatus}
                    style={{
                      fontWeight: "bolder",
                      fontSize: 22,
                      alignItems: "center",
                      textTransform: "capitalize",
                    }}
                  />
                </FormItem>
                <FormItem
                  label={
                    <Label
                      style={{
                        alignSelf: "center",
                        fontSize: contentLabelSize,
                      }}
                    >
                      Package name
                    </Label>
                  }
                >
                  <Label
                    style={{ alignSelf: "center", fontSize: contentLabelSize }}
                  >
                    {appDetails?.appPackage}
                  </Label>
                </FormItem>
                <FormItem
                  label={
                    <Label
                      style={{
                        alignSelf: "center",
                        fontSize: contentLabelSize,
                      }}
                    >
                      Category
                    </Label>
                  }
                >
                  <Label
                    style={{ alignSelf: "center", fontSize: contentLabelSize }}
                  >
                    {appDetails?.category}
                  </Label>
                </FormItem>
                <FormItem
                  label={
                    <Label
                      style={{
                        alignSelf: "center",
                        fontSize: contentLabelSize,
                      }}
                    >
                      Supported Platform
                    </Label>
                  }
                >
                  <DisplaySupportedPlatform
                    supportedPlatform={appDetails?.supportedPlatform}
                  />
                </FormItem>
                <FormItem
                  label={
                    <Label
                      style={{
                        alignSelf: "center",
                        fontSize: contentLabelSize,
                      }}
                    >
                      Generated At
                    </Label>
                  }
                >
                  <Label
                    style={{ alignSelf: "center", fontSize: contentLabelSize }}
                  >
                    {appDetails?.triggeredTime
                      ? formatTriggeredAt(appDetails?.triggeredTime)
                      : ""}
                  </Label>
                </FormItem>
              </Form>
              <MessageStrip
                hidden={moreInformation.msg ? undefined : true}
                hideCloseButton
                design={moreInformation?.design}
              >
                {moreInformation.msg}
              </MessageStrip>
            </FlexBox>
          </div>
          <div
            style={{
              gridColumn: "span 5",
              margin: "5px",
            }}
          >
            <TestInformation
              expoBuildUrl={
                appDetails?.expoBuildUrl ? appDetails?.expoBuildUrl : ""
              }
              appName={appDetails?.appName}
              buildStatus={appDetails?.pipelineStatus}
            />
          </div>
        </ResponsiveGridLayout>
      )}
    </>
  );
  //   }
}
