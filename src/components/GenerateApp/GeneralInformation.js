import React, { useState, useEffect } from "react";
import {
  Avatar,
  ComboBox,
  ComboBoxItem,
  CheckBox,
  Card,
  Text,
  Label,
  Icon,
  Input,
  Form,
  FormItem,
  FlexBox,
  ResponsiveGridLayout,
  FileUploader,
} from "@ui5/webcomponents-react";
import axios from "../../api/axios";

export default function GeneralInformation(props) {
  const { updatedData, setUpdatedData, setIcon } = props;
  const [iconUrl, setIconUrl] = useState("");
  const onImageUpload = (event) => {
    if (event.target && event.target.files.length > 0) {
      setIcon(event.target.files[0]);
      var url = URL.createObjectURL(event.target.files[0]);
      setIconUrl(url);
    }
  };
  const onNameChange = (event) => {
    setUpdatedData({ ...updatedData, appName: event.target.value });
  };
  const onCategoryChange = (event) => {
    setUpdatedData({ ...updatedData, appCategory: event.target.value });
  };
  const onSupportedPltfrmChange = (event) => {
    switch (event.target.id) {
      case "android":
        setUpdatedData({
          ...updatedData,
          supportedPlatform: {
            ...updatedData?.supportedPlatform,
            android: event.target.checked,
          },
        });
        break;
      case "ios":
        setUpdatedData({
          ...updatedData,
          supportedPlatform: {
            ...updatedData?.supportedPlatform,
            ios: event.target.checked,
          },
        });
        break;
      case "web":
        setUpdatedData({
          ...updatedData,
          supportedPlatform: {
            ...updatedData?.supportedPlatform,
            web: event.target.checked,
          },
        });
        break;
      default:
        break;
    }
  };
  useEffect(() => {
    axios
      .get("/appDetails")
      .then((response) => {
        setUpdatedData({
          appName: response?.data?.appName,
          appCategory: response?.data?.category,
          supportedPlatform: response?.data?.supportedPlatform,
        });
        if (response?.data?.appIcon) {
          setIconUrl(response?.data?.appIcon);
        }
      })
      .catch(() => {});
  }, []);
  return (
    <Form style={{ margin: "40px" }}>
      <FormItem label="Appplication Name">
        <Input
          style={{ margin: "10px" }}
          value={updatedData?.appName}
          onChange={onNameChange}
        />
      </FormItem>
      <FormItem
        label={<Label style={{ alignSelf: "center" }}>Appplication Icon</Label>}
      >
        <FileUploader hideInput onChange={onImageUpload} accept=".jpeg,.png">
          <Icon name="touch" style={{ margin: "5px", width: 30, height: 30 }} />
        </FileUploader>
        {iconUrl ? (
          <Avatar
            icon="employee"
            size="XS"
            style={{ marginLeft: "20px" }}
            children={<img src={iconUrl} alt=""></img>}
          />
        ) : (
          <></>
        )}
      </FormItem>
      <FormItem label="Appplication Category">
        <ComboBox
          value={updatedData?.appCategory}
          style={{ margin: "10px" }}
          onSelectionChange={onCategoryChange}
        >
          <ComboBoxItem text="Grocerry" />
          <ComboBoxItem text="Electronics" />
          <ComboBoxItem text="Fashion" />
          <ComboBoxItem text="Food" />
          <ComboBoxItem text="Others" />
        </ComboBox>
      </FormItem>
      <FormItem
        label={
          <Label style={{ alignSelf: "center" }}>Supported Platforms</Label>
        }
      >
        <ResponsiveGridLayout columnGap="1rem" style={{ margin: "10px" }}>
          <div
            style={{
              gridColumn: "span 3",
            }}
          >
            <Card>
              <FlexBox justifyContent={"Center"} alignItems={"Center"}>
                <CheckBox
                  id="android"
                  checked={updatedData?.supportedPlatform?.android}
                  onChange={onSupportedPltfrmChange}
                />
                <FlexBox
                  id="android--div"
                  direction={"Column"}
                  justifyContent={"Center"}
                  alignItems={"Center"}
                >
                  <Icon
                    id="android--icon"
                    name="ipad"
                    style={{ width: 42, height: 42, margin: "5px" }}
                  />
                  <Text
                    id="android--text"
                    style={{
                      margin: "5px",
                      fontWeight: "bolder",
                      fontFamily: "inherit",
                      fontSize: 16,
                    }}
                  >
                    Android
                  </Text>
                </FlexBox>
              </FlexBox>
            </Card>
          </div>
          <div
            style={{
              gridColumn: "span 3",
            }}
          >
            <Card>
              <FlexBox justifyContent={"Center"} alignItems={"Center"}>
                <CheckBox
                  id="ios"
                  checked={updatedData?.supportedPlatform?.ios}
                  onChange={onSupportedPltfrmChange}
                />
                <FlexBox
                  id="ios--div"
                  direction={"Column"}
                  justifyContent={"Center"}
                  alignItems={"Center"}
                >
                  <Icon
                    id="ios--icon"
                    name="iphone"
                    style={{ width: 42, height: 42, margin: "5px" }}
                  />
                  <Text
                    id="ios--text"
                    style={{
                      margin: "5px",
                      fontWeight: "bolder",
                      fontFamily: "inherit",
                      fontSize: 16,
                    }}
                  >
                    IOS
                  </Text>
                </FlexBox>
              </FlexBox>
            </Card>
          </div>
          <div
            style={{
              gridColumn: "span 3",
            }}
          >
            <Card>
              <FlexBox justifyContent={"Center"} alignItems={"Center"}>
                <CheckBox
                  id="web"
                  checked={updatedData?.supportedPlatform?.web}
                  onChange={onSupportedPltfrmChange}
                />
                <FlexBox
                  id="web--div"
                  direction={"Column"}
                  justifyContent={"Center"}
                  alignItems={"Center"}
                >
                  <Icon
                    id="web--icon"
                    name="laptop"
                    style={{ width: 42, height: 42, margin: "5px" }}
                  />
                  <Text
                    id="web--text"
                    style={{
                      margin: "5px",
                      fontWeight: "bolder",
                      fontFamily: "inherit",
                      fontSize: 16,
                    }}
                  >
                    Web
                  </Text>
                </FlexBox>
              </FlexBox>
            </Card>
          </div>
        </ResponsiveGridLayout>
      </FormItem>
    </Form>
  );
}
