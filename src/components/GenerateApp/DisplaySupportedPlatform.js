import React from "react";
import {
  ResponsiveGridLayout,
  Card,
  FlexBox,
  Icon,
  Text,
} from "@ui5/webcomponents-react";
export default function DisplaySupportedPlatform(props) {
  const { supportedPlatform } = props;
  return (
    <ResponsiveGridLayout columnGap="1rem" style={{ margin: "10px" }}>
      <div
        style={{
          gridColumn: "span 3",
        }}
        hidden={!supportedPlatform?.android}
      >
        <Card>
          <FlexBox justifyContent={"Center"} alignItems={"Center"}>
            <FlexBox
              id="android--div"
              direction={"Column"}
              justifyContent={"Center"}
              alignItems={"Center"}
            >
              <Icon
                id="android--icon"
                name="ipad"
                style={{ width: 42, height: 42, margin: "5px" }}
              />
              <Text
                id="android--text"
                style={{
                  margin: "5px",
                  fontWeight: "bolder",
                  fontFamily: "inherit",
                  fontSize: 16,
                }}
              >
                Android
              </Text>
            </FlexBox>
          </FlexBox>
        </Card>
      </div>
      <div
        style={{
          gridColumn: "span 3",
        }}
        hidden={!supportedPlatform?.ios}
      >
        <Card>
          <FlexBox justifyContent={"Center"} alignItems={"Center"}>
            <FlexBox
              id="ios--div"
              direction={"Column"}
              justifyContent={"Center"}
              alignItems={"Center"}
            >
              <Icon
                id="ios--icon"
                name="iphone"
                style={{ width: 42, height: 42, margin: "5px" }}
              />
              <Text
                id="ios--text"
                style={{
                  margin: "5px",
                  fontWeight: "bolder",
                  fontFamily: "inherit",
                  fontSize: 16,
                }}
              >
                IOS
              </Text>
            </FlexBox>
          </FlexBox>
        </Card>
      </div>
      <div
        style={{
          gridColumn: "span 3",
        }}
        hidden={!supportedPlatform?.web}
      >
        <Card>
          <FlexBox justifyContent={"Center"} alignItems={"Center"}>
            <FlexBox
              id="web--div"
              direction={"Column"}
              justifyContent={"Center"}
              alignItems={"Center"}
            >
              <Icon
                id="web--icon"
                name="laptop"
                style={{ width: 42, height: 42, margin: "5px" }}
              />
              <Text
                id="web--text"
                style={{
                  margin: "5px",
                  fontWeight: "bolder",
                  fontFamily: "inherit",
                  fontSize: 16,
                }}
              >
                Web
              </Text>
            </FlexBox>
          </FlexBox>
        </Card>
      </div>
    </ResponsiveGridLayout>
  );
}
