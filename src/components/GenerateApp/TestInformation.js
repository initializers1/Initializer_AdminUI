import React, { useRef } from "react";
import {
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxDirection,
  FlexBoxJustifyContent,
  Input,
  Text,
  Toast,
  Button,
  ButtonDesign,
  Icon,
  Link,
} from "@ui5/webcomponents-react";
import QRCode from "qrcode.react";
import { ExpoAppStoreURL, ExpoPlayStoreURL } from "../Constants/AppConstants";

import "@ui5/webcomponents-icons/dist/copy.js";
export default function TestInformation(props) {
  const { expoBuildUrl, appName, buildStatus } = props;
  const copyToast = useRef();
  const onCopyToClipBoard = (event) => {
    var text = event.target?.parentElement?.value;
    if ("clipboard" in navigator) {
      return navigator.clipboard.writeText(text).then(() => {
        copyToast.current.show();
      });
    } else {
      document.execCommand("copy", true, text);
      copyToast.current.show();
    }
  };
  const getRunAppOnYourDeviceButton = () => {
    const buttonLabel = `Open ${appName}`;
    if (window.innerWidth <= 768 && window.innerHeight <= 1024) {
      return (
        <Button
          design={ButtonDesign.Emphasized}
          style={{ margin: "10px" }}
          onClick={() => {
            window.open(expoBuildUrl, "_blank");
          }}
        >
          {buttonLabel}
        </Button>
      );
    }
  };
  return (
    <div
      style={{
        margin: "10px",
      }}
      hidden={buildStatus === "success" ? false : true}
    >
      <FlexBox
        justifyContent={FlexBoxJustifyContent.SpaceBetween}
        direction={FlexBoxDirection.Column}
      >
        <FlexBox>
          <Text style={{ fontWeight: "bold" }}>Test your Application</Text>
        </FlexBox>
        <FlexBox
          direction={FlexBoxDirection.Column}
          justifyContent={FlexBoxJustifyContent.Center}
          alignItems={FlexBoxAlignItems.Center}
        >
          <img
            src={`http://api.qrserver.com/v1/create-qr-code/?data=${expoBuildUrl}!&size=150x150`}
            style={{margin: '10px'}}
          ></img>
          <Input
            icon={
              <Icon
                name="copy"
                onClick={onCopyToClipBoard}
                tooltip="copy link"
              />
            }
            readonly
            value={expoBuildUrl}
            style={{ margin: "10px", width: "60%" }}
          ></Input>
          {getRunAppOnYourDeviceButton()}
        </FlexBox>
        <FlexBox direction={FlexBoxDirection.Column}>
          <Text style={{ fontWeight: "bold" }}>
            To Test on an Android device
          </Text>
          <Text>
            <ol>
              <li>
                Install{" "}
                <Link href={ExpoPlayStoreURL} target="_blank">
                  Expo
                </Link>{" "}
                from your Play Store
              </li>
              <li>Open Expo app and scan above QR Code</li>
              <li>
                Start exploring your App by adding more Items and more Widget to
                your Home Page
              </li>
            </ol>
          </Text>
          <Text style={{ fontWeight: "bold" }}>To Test on an IOS device</Text>
          <Text>
            <ol>
              <li>
                Install{" "}
                <Link href={ExpoAppStoreURL} target="_blank">
                  Expo Go
                </Link>{" "}
                from your App Store
              </li>
              <li>Open your browser and copy paste the above URL</li>
              <li>
                Start exploring your App by adding more Items and more Widget to
                your Home Page
              </li>
            </ol>
          </Text>
        </FlexBox>
      </FlexBox>
      <Toast ref={copyToast}>Copied</Toast>
    </div>
  );
}
