import React from "react";

import { CheckBox, FlexBox } from "@ui5/webcomponents-react";
export default function TermsInformation(props) {
  const { setAgreeTerms } = props;
  const onChangeAgree = (event) => {
    setAgreeTerms(event.target.checked);
  };
  return (
    <FlexBox justifyContent={"Center"} alignItems={"Center"}>
      <CheckBox
        text="I hearby agree to all Terms and Condition"
        onChange={onChangeAgree}
      />
    </FlexBox>
  );
}
