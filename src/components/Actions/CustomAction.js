import React from 'react';

import CustomAvailabilityAction from './CustomAvailabilityAction.js'
export default function CustomAction(props) {
    const actionType = props.actionProp.type;
    switch (actionType) {
        case "CustomAvailabilityAction":
            return (
                <CustomAvailabilityAction actionUrl={props.actionProp.url} />
            );
        default:
            break;
    }
}