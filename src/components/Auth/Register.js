import React, { useEffect, useState } from "react";
import {
  Card,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxJustifyContent,
  Avatar,
  AvatarShape,
  Text,
  AvatarSize,
  FlexBoxDirection,
  MessageStrip,
  MessageStripDesign,
} from "@ui5/webcomponents-react";
import LuigiClient, { sendCustomMessage } from "@luigi-project/client";
import BasicDetails from "./Register/BasicDetails";
import SecondryAuth from "./Register/SecondryAuth";
import Axios from "axios";
import { AppName } from "../AppConstants";
export default function Register() {
  const [registrationState, setRegistrationState] = useState("BASIC");
  const [errorMessage, setErrorMessage] = useState("");
  const [errorMessageState, setErrorMessageState] = useState(
    MessageStripDesign.Information
  );
  useEffect(() => {
    const otpExipryTimeFromStorage = localStorage.getItem(
      "initializers.otp.expiryTime"
    );
    if (otpExipryTimeFromStorage) {
      const otpExpiryTime = new Date(otpExipryTimeFromStorage);
      if (otpExpiryTime - Date.now() > 0) setRegistrationState("OTP");
    }
  });
  const triggerRegistration = (userName, password, storeName) => {
    setErrorMessage("");
    LuigiClient.uxManager().showLoadingIndicator();
    Axios.post(`${process.env.REACT_APP_DOMAIN}/user/register`, {
      email: userName,
      password: password,
      appName: storeName,
    })
      .then((response) => {
        if (response.data) {
          localStorage.setItem("initializers.user.id", response.data.userId);
          const resendOTPMaxTime = parseInt(response.data.otpExpiryTime)
            ? parseInt(response.data.otpExpiryTime) + 10
            : 90;
          localStorage.setItem(
            "initializers.otp.resendOTPMaxTime",
            resendOTPMaxTime
          );
          const otpExipryTime = new Date();
          otpExipryTime.setSeconds(
            otpExipryTime.getSeconds() + resendOTPMaxTime
          );
          localStorage.setItem("initializers.otp.expiryTime", otpExipryTime);
          initializeOtpScreen(userName);
        } else {
          displayError();
        }
        LuigiClient.uxManager().hideLoadingIndicator();
      })
      .catch((error) => {
        displayError(
          error.response && error.response.data
            ? error.response.data.message
            : undefined,
          MessageStripDesign.Negative
        );
        LuigiClient.uxManager().hideLoadingIndicator();
      });
  };
  const triggerOtpValidation = (otp) => {
    LuigiClient.uxManager().showLoadingIndicator();
    setErrorMessage("");
    Axios.post(`${process.env.REACT_APP_DOMAIN}/user/otp`, {
      id: localStorage.getItem("initializers.user.id"),
      otp,
    })
      .then((response) => {
        if (response.data) {
          clearLocalStorage();
          sendCustomMessage({
            id: "initializers.login",
            data: response.data,
          });
        } else {
          displayError();
        }
        LuigiClient.uxManager().hideLoadingIndicator();
      })
      .catch((error) => {
        displayError(
          error.response && error.response.data
            ? error.response.data.message
            : undefined,
          MessageStripDesign.Negative
        );
        LuigiClient.uxManager().hideLoadingIndicator();
      });
  };
  const triggerResendOtp = () => {
    LuigiClient.uxManager().showLoadingIndicator();
    setErrorMessage("");
    const userId = localStorage.getItem("initializers.user.id");
    if (userId) {
      return Axios.get(`${process.env.REACT_APP_DOMAIN}/user/otp/${userId}`);
    }
  };
  const initializeOtpScreen = (userName) => {
    setRegistrationState("OTP");
    displayError(
      `Please provide OTP to complete Registration, We have sent an OTP to your email ID ${userName}`,
      MessageStripDesign.Information
    );
  };
  const displayError = (msg, type) => {
    setErrorMessageState(type ? type : MessageStripDesign.Negative);
    setErrorMessage(
      msg
        ? msg
        : "An error encontered while creating your store, please try again later"
    );
  };
  const onGoBackFromOtp = () => {
    clearLocalStorage();
    setErrorMessage("");
    setRegistrationState("Basic");
  };
  const clearLocalStorage = () => {
    localStorage.removeItem("initializers.user.id");
    localStorage.removeItem("initializers.otp.expiryTime");
  };
  return (
    <FlexBox
      justifyContent={FlexBoxJustifyContent.Center}
      alignItems={FlexBoxAlignItems.Center}
      direction={FlexBoxDirection.Column}
      style={{ height: "100vh" }}
    >
      <Card
        header={
          <FlexBox
            style={{
              paddingLeft: "20px",
              paddingTop: "20px",
              paddingBottom: "10px",
            }}
            justifyContent={FlexBoxJustifyContent.Start}
            direction={FlexBoxDirection.Column}
          >
            <FlexBox direction={FlexBoxDirection.Row}>
              <Avatar
                colorScheme="Accent6"
                icon="employee"
                size={AvatarSize.XS}
                shape={AvatarShape.Square}
                children={
                  <img
                    alt={"initializers logo"}
                    src={
                      "https://res.cloudinary.com/dsywyhhdl/image/upload/v1590678445/Capture_tndec6.png"
                    }
                    style={{ objectFit: "fill" }}
                  ></img>
                }
              />
              <Text
                style={{
                  fontWeight: "bolder",
                  fontSize: 26,
                  marginLeft: "10px",
                }}
              >
                {AppName}
              </Text>
            </FlexBox>
            <Text
              style={{ fontWeight: "bold", fontSize: 20, marginTop: "20px" }}
            >
              Sign Up
            </Text>
          </FlexBox>
        }
        style={{
          maxWidth: "380px",
        }}
      >
        <MessageStrip
          hideCloseButton={true}
          design={errorMessageState}
          hidden={errorMessage ? null : true}
          style={{ padding: "5px", width: "auto" }}
        >
          {errorMessage}
        </MessageStrip>
        {(() => {
          switch (registrationState) {
            case "OTP":
              return (
                <SecondryAuth
                  triggerOtpValidation={triggerOtpValidation}
                  triggerResendOtp={triggerResendOtp}
                  onGoBackFromOtp={onGoBackFromOtp}
                />
              );
            default:
              return <BasicDetails triggerRegistration={triggerRegistration} />;
          }
        }).call(this)}
      </Card>
    </FlexBox>
  );
}
