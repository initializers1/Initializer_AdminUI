import React, { useState } from "react";
import {
  CheckBox,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxJustifyContent,
  Text,
  FlexBoxDirection,
  Form,
  FormItem,
  Input,
  InputType,
  Link,
  Button,
  ButtonDesign,
  ValueState,
} from "@ui5/webcomponents-react";
import LuigiClient from "@luigi-project/client";
export default function BasicDetails(props) {
  const { triggerRegistration } = props;
  const [storeName, setStoreName] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [invalidUsername, setInvalidUserName] = useState(false);
  const [invalidPassword, setInvalidPassword] = useState(false);
  const [invalidStoreName, setinvalidStoreName] = useState(false);
  const onRegister = () => {
    if (!checkInput()) return;
    triggerRegistration(username, password, storeName);
  };
  const checkInput = () => {
    if (!storeName) {
      document.getElementById("registerpage--storename").focus();
      setinvalidStoreName(true);
      return false;
    } else {
      setinvalidStoreName(false);
    }
    if (!validateEmail()) {
      document.getElementById("registerpage--username").focus();
      setInvalidUserName(true);
      return false;
    } else {
      setInvalidUserName(false);
    }

    if (!password) {
      document.getElementById("registerpage--password").focus();
      setInvalidPassword(true);
      return false;
    } else {
      setInvalidPassword(false);
    }
    return true;
  };
  const validateEmail = () => {
    var emailRegex = /\S+@\S+\.\S+/;
    return emailRegex.test(username);
  };
  const onKeyPress = (event) => {
    if (event.charCode === 13) {
      onRegister();
    }
  };
  const onLogin = () => {
    LuigiClient.linkManager().navigate("/login");
  };
  return (
    <>
      <FlexBox
        justifyContent={FlexBoxJustifyContent.Center}
        alignItems={FlexBoxAlignItems.Start}
        style={{ marginTop: "10px", marginLeft: "10px", marginRight: "10px" }}
        direction={FlexBoxDirection.Column}
      >
        <Form style={{width: '100%'}}>
          <FormItem>
            <Input
              id={"registerpage--storename"}
              type={InputType.Email}
              placeholder="Store Name"
              value={storeName}
              onInput={(event) => setStoreName(event.target.value)}
              valueState={invalidStoreName ? ValueState.Error : ValueState.None}
              onKeyPress={onKeyPress}
              style={{
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderTopWidth: 0,
                borderBottomWidth: 2,
                width: '100%',
                margin: "5px",
              }}
            />
          </FormItem>
          <FormItem>
            <Input
              id={"registerpage--username"}
              type={InputType.Email}
              placeholder="Email"
              value={username}
              onInput={(event) => setUsername(event.target.value)}
              valueState={invalidUsername ? ValueState.Error : ValueState.None}
              onKeyPress={onKeyPress}
              style={{
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderTopWidth: 0,
                borderBottomWidth: 2,
                width: '100%',
                margin: "5px",
              }}
            />
          </FormItem>
          <FormItem>
            <Input
              id={"registerpage--password"}
              type={showPassword ? InputType.Text : InputType.Password}
              placeholder={"Password"}
              value={password}
              valueState={invalidPassword ? ValueState.Error : ValueState.None}
              onInput={(event) => setPassword(event.target.value)}
              onKeyPress={onKeyPress}
              style={{
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderTopWidth: 0,
                borderBottomWidth: 2,
                width: '100%',
                marginTop: "5px",
                marginRight: "5px",
                marginLeft: "5px",
              }}
            />
          </FormItem>
          <FormItem>
            <CheckBox
              onChange={() => setShowPassword(!showPassword)}
              checked={showPassword}
              text="Show Password"
            />
          </FormItem>
        </Form>
      </FlexBox>
      <FlexBox direction={FlexBoxDirection.Column} style={{ padding: "10px" }}>
        <FlexBox direction={FlexBoxDirection.Row} style={{ margin: "5px" }}>
          <Text style={{ marginRight: "4px" }}>Already have an account?</Text>
          <Link onClick={onLogin}>Login</Link>
        </FlexBox>
        <FlexBox justifyContent={FlexBoxJustifyContent.End}>
          <Button design={ButtonDesign.Emphasized} onClick={onRegister}>
            Continue
          </Button>
        </FlexBox>
      </FlexBox>
    </>
  );
}
