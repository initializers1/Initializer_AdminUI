import React, { useEffect, useRef, useState } from "react";
import {
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxJustifyContent,
  FlexBoxDirection,
  Form,
  FormItem,
  Input,
  InputType,
  Button,
  ButtonDesign,
  ValueState,
  Text,
  Link,
  Toast,
} from "@ui5/webcomponents-react";
import { resendOTPMaxTime } from "../../AppConstants";
import LuigiClient from "@luigi-project/client";
export default function SecondryAuth(props) {
  const { triggerOtpValidation, triggerResendOtp, onGoBackFromOtp } = props;
  const [otp, setOtp] = useState("");
  const [invalidOtp, setinvalidOtp] = useState(false);
  const [enableResend, setEnableResend] = useState(false);
  const [counter, setCounter] = useState({ min: 0, sec: 0 });
  const toastMessage = useRef();
  const resendOTPMaxTime = parseInt(localStorage.getItem("initializers.otp.resendOTPMaxTime"));
  var otpExpiryTime = new Date(
    localStorage.getItem("initializers.otp.expiryTime")
  );
  if (!otpExpiryTime.getDate()) {
    otpExpiryTime = new Date();
    otpExpiryTime.setSeconds(otpExpiryTime.getSeconds() + resendOTPMaxTime);
  }
  const totalTime = useRef(otpExpiryTime);
  const onSubmitOtp = () => {
    if (!otp) {
      setinvalidOtp(true);
      return;
    } else {
      triggerOtpValidation(otp);
    }
  };
  const onResendOtp = () => {
    triggerResendOtp().then(() => {
      setEnableResend(false);
      const now = new Date();
      now.setSeconds(now.getSeconds() + resendOTPMaxTime);
      totalTime.current = now;
      changeRemainingTime();
      localStorage.setItem("initializers.otp.expiryTime", now);
      toastMessage.current.show();
      LuigiClient.uxManager().hideLoadingIndicator();
    });
  };
  const onKeyPress = (event) => {
    if (event.charCode === 13) {
      onSubmitOtp();
    }
  };
  useEffect(() => {
    setTimeout(() => {
      changeRemainingTime();
    }, 1000);
  }, [counter]);
  const changeRemainingTime = () => {
    var remainingTime = totalTime.current - new Date();
    if (remainingTime > 0) {
      var sec = Math.floor((remainingTime / 1000) % 60);
      var min = Math.floor((remainingTime / 1000 / 60) % 60);
      setCounter({
        min: String(min).padStart(2, "0"),
        sec: String(sec).padStart(2, "0"),
      });
    } else {
      setEnableResend(true);
    }
  };
  return (
    <>
      <FlexBox
        justifyContent={FlexBoxJustifyContent.Center}
        alignItems={FlexBoxAlignItems.Start}
        style={{ marginTop: "10px", marginLeft: "10px", marginRight: "10px" }}
        direction={FlexBoxDirection.Column}
      >
        <Form style={{ width: "100%" }}>
          <FormItem>
            <Input
              id={"registerpage--storename"}
              type={InputType.Email}
              placeholder="OTP"
              value={otp}
              onInput={(event) => setOtp(event.target.value)}
              valueState={invalidOtp ? ValueState.Error : ValueState.None}
              onKeyPress={onKeyPress}
              style={{
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderTopWidth: 0,
                borderBottomWidth: 2,
                width: "100%",
                margin: "5px",
              }}
            />
          </FormItem>
          <FormItem>
            {enableResend ? (
              <></>
            ) : (
              <Text
                style={{
                  margin: "5px",
                  fontStyle: "initial",
                  fontWeight: "bold",
                }}
              >
                {`${counter.min}: ${counter.sec}`}
              </Text>
            )}
            <Link
              style={{ margin: "5px" }}
              onClick={onResendOtp}
              hidden={enableResend ? null : true}
            >
              Resend OTP
            </Link>
          </FormItem>
        </Form>
      </FlexBox>
      <FlexBox
        direction={FlexBoxDirection.Row}
        justifyContent={FlexBoxJustifyContent.SpaceBetween}
        style={{ padding: "10px" }}
      >
        <FlexBox>
          <Button design={ButtonDesign.Default} onClick={onGoBackFromOtp}>
            Go Back
          </Button>
        </FlexBox>
        <FlexBox justifyContent={FlexBoxJustifyContent.End}>
          <Button design={ButtonDesign.Emphasized} onClick={onSubmitOtp}>
            Confirm
          </Button>
        </FlexBox>
      </FlexBox>
      <Toast ref={toastMessage}>OTP sent successfully</Toast>
    </>
  );
}
