import React, { useState } from "react";
import {
  Card,
  CheckBox,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxJustifyContent,
  Avatar,
  AvatarShape,
  Text,
  AvatarSize,
  FlexBoxDirection,
  Form,
  FormItem,
  Input,
  InputType,
  MessageStrip,
  MessageStripDesign,
  Link,
  Button,
  ButtonDesign,
  ValueState,
} from "@ui5/webcomponents-react";
import LuigiClient, { sendCustomMessage } from "@luigi-project/client";
import Axios from "axios";
export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [invalidUsername, setInvalidUserName] = useState(false);
  const [invalidPassword, setInvalidPassword] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const onLogin = () => {
    setErrorMessage("");
    if (!checkInput()) return;
    LuigiClient.uxManager().showLoadingIndicator();
    Axios.post(`${process.env.REACT_APP_DOMAIN}/user/login`, {
      username: username,
      password: password,
    })
      .then((response) => {
        if (response.data) {
          sendCustomMessage({
            id: "initializers.login",
            data: response.data,
          });
        }
        LuigiClient.uxManager().hideLoadingIndicator();
      })
      .catch((error) => {
        setErrorMessage("Username or password is incorrect, Please try again");
        LuigiClient.uxManager().hideLoadingIndicator();
      });
  };
  const checkInput = () => {
    if (!validateEmail()) {
      document.getElementById("loginpage--username").focus();
      setInvalidUserName(true);
      return false;
    } else {
      setInvalidUserName(false);
    }

    if (!password) {
      document.getElementById("loginpage--password").focus();
      setInvalidPassword(true);
      return false;
    } else {
      setInvalidPassword(false);
    }
    return true;
  };
  const validateEmail = () => {
    var emailRegex = /\S+@\S+\.\S+/;
    return emailRegex.test(username);
  };
  const onKeyPress = (event) => {
    if (event.charCode === 13) {
      onLogin();
    }
  };
  const onRegister = () => {
    LuigiClient.linkManager().navigate("/register");
  };
  return (
    <FlexBox
      justifyContent={FlexBoxJustifyContent.Center}
      alignItems={FlexBoxAlignItems.Center}
      style={{ height: "100vh" }}
    >
      <Card
        header={
          <FlexBox
            style={{
              paddingLeft: "20px",
              paddingTop: "20px",
              paddingBottom: "10px",
            }}
            justifyContent={FlexBoxJustifyContent.Start}
            direction={FlexBoxDirection.Column}
          >
            <FlexBox direction={FlexBoxDirection.Row}>
              <Avatar
                colorScheme="Accent6"
                icon="employee"
                size={AvatarSize.XS}
                shape={AvatarShape.Square}
                children={
                  <img
                    alt={"item details"}
                    src={
                      "https://res.cloudinary.com/dsywyhhdl/image/upload/v1590678445/Capture_tndec6.png"
                    }
                    style={{ objectFit: "fill" }}
                  ></img>
                }
              />
              <Text
                style={{
                  fontWeight: "bolder",
                  fontSize: 26,
                  marginLeft: "10px",
                }}
              >
                Initializers
              </Text>
            </FlexBox>
            <Text
              style={{ fontWeight: "bold", fontSize: 20, marginTop: "20px" }}
            >
              Sign In
            </Text>
          </FlexBox>
        }
        style={{
          width: "380px",
        }}
      >
        <FlexBox
          justifyContent={FlexBoxJustifyContent.Center}
          alignItems={FlexBoxAlignItems.Start}
          style={{ marginTop: "10px", marginLeft: "10px", marginRight: "10px" }}
          direction={FlexBoxDirection.Column}
        >
          <MessageStrip
            hideCloseButton={true}
            design={MessageStripDesign.Negative}
            hidden={errorMessage ? null : true}
          >
            {errorMessage}
          </MessageStrip>
          <Form style={{ width: "100%" }}>
            <FormItem>
              <Input
                id={"loginpage--username"}
                type={InputType.Email}
                placeholder="Email"
                value={username}
                onInput={(event) => setUsername(event.target.value)}
                valueState={
                  invalidUsername ? ValueState.Error : ValueState.None
                }
                onKeyPress={onKeyPress}
                style={{
                  borderLeftWidth: 0,
                  borderRightWidth: 0,
                  borderTopWidth: 0,
                  borderBottomWidth: 2,
                  width: "100%",
                  margin: "5px",
                }}
              />
            </FormItem>
            <FormItem>
              <Input
                id={"loginpage--password"}
                type={showPassword ? InputType.Text : InputType.Password}
                placeholder={"Password"}
                value={password}
                valueState={
                  invalidPassword ? ValueState.Error : ValueState.None
                }
                onInput={(event) => setPassword(event.target.value)}
                onKeyPress={onKeyPress}
                style={{
                  borderLeftWidth: 0,
                  borderRightWidth: 0,
                  borderTopWidth: 0,
                  borderBottomWidth: 2,
                  width: "100%",
                  marginTop: "5px",
                  marginRight: "5px",
                  marginLeft: "5px",
                }}
              />
            </FormItem>
            <FormItem>
              <CheckBox
                onChange={() => setShowPassword(!showPassword)}
                checked={showPassword}
                text="Show Password"
              />
            </FormItem>
          </Form>
        </FlexBox>
        <FlexBox
          direction={FlexBoxDirection.Column}
          style={{ padding: "10px" }}
        >
          <FlexBox direction={FlexBoxDirection.Row} style={{ margin: "5px" }}>
            <Text style={{ marginRight: "4px" }}>No Account?</Text>
            <Link onClick={onRegister}>Create Account</Link>
          </FlexBox>
          <FlexBox>
            <Link style={{ margin: "5px" }}>Forgot Password?</Link>
          </FlexBox>
          <FlexBox justifyContent={FlexBoxJustifyContent.End}>
            <Button design={ButtonDesign.Emphasized} onClick={onLogin}>
              Continue
            </Button>
          </FlexBox>
        </FlexBox>
      </Card>
    </FlexBox>
  );
}
