import React from "react";
import { TableRow, TableCell } from "@ui5/webcomponents-react";

import CustomColumnItem from "../CustomColumnItem.js";

export default function CustomRow(props) {
  const rows = props.rowData === undefined ? [] : props.rowData;
  return (
    <>
      {rows.map((row) => (
        <TableRow key={row["id"]} id={row["id"]} type={"Active"}>
          {props.columnName.map((column) => (
            <TableCell key={`TableCell--${column}`} id={"id"}>
              <CustomColumnItem
                key={`CustomColumnItem--${column}`}
                value={row[column]}
                property={props.property[column]}
                criticality={
                  props.property[column]
                    ? props.property[column].criticality
                    : []
                }
              />
            </TableCell>
          ))}
          <TableCell className="fd-table__cell fd-table__cell--fit-content fd-table__cell--no-padding">
            <i
              className="fd-table__icon fd-table__icon--navigation sap-icon--slim-arrow-right"
              role="presentation"
            ></i>
          </TableCell>
          {/* <td className="fd-table__cell fd-table__cell--fit-content fd-table__cell--no-padding">
            <i
              className="fd-table__icon fd-table__icon--navigation sap-icon--slim-arrow-right"
              role="presentation"
            ></i>
          </td> */}
        </TableRow>
      ))}
    </>
  );
}
