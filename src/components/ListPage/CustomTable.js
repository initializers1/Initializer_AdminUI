import React, { useEffect, useState } from "react";
import {
  Table,
  TableGrowingMode,
  BusyIndicator,
  FlexBox
} from "@ui5/webcomponents-react";
import CustomColumn from "./CustomColumn";
import CustomRow from "./CustomRow";
import Axios from "../../api/axios";
import constructFilterValue from "../../functions/constructFilterValue";

export default function CustomTable(props) {
  const {
    property,
    listPage,
    onRowClick,
    context,
    filterValue,
    onSelectionChange,
    tableData,
    setEntityData,
  } = props;
  let columnData = listPage.fields === undefined ? [] : listPage.fields;
  let mode = listPage.type ? listPage.type : "None";
  const [pageDetails, setPageDetails] = useState(props.initialPageDetails);

  useEffect(() => {
    setPageDetails(props.initialPageDetails);
  }, [props.initialPageDetails]);

  const onLoadMore = () => {
    if (pageDetails && !pageDetails.last && pageDetails.pageable) {
      let filter = constructFilterValue(filterValue);
      Axios.get(
        `${context}${filter}&offset=${
          parseInt(pageDetails.pageable.pageNumber) + 1
        }&count=${process.env.REACT_APP_LIST_PAGE_SIZE}`
      )
        .then((response) => {
          if (response.data) {
            setEntityData([...tableData, ...response.data.content]);
            setPageDetails({
              last: response.data.last,
              pageable: response.data.pageable,
              totalElements: response.data.totalElements,
            });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  return (
    <>
      <Table
        noDataText={"no Items avaialble"}
        stickyColumnHeader={props.isDerived ? false : true}
        columns={<CustomColumn columnName={columnData} property={property} />}
        mode={mode}
        onLoadMore={onLoadMore}
        growing={TableGrowingMode.Scroll}
        onRowClick={onRowClick}
        onSelectionChange={onSelectionChange}
      >
        <CustomRow
          rowData={tableData}
          columnName={columnData}
          property={property}
        />
      </Table>
      <FlexBox style={{ justifyContent: "center" }}>
        <BusyIndicator
          active={
            pageDetails && pageDetails.last !== undefined && !pageDetails.last
          }
        />
      </FlexBox>
    </>
  );
}
