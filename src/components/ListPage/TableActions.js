import React, { useRef, useState } from "react";
import {
  Toolbar,
  Button,
  ButtonDesign,
  ToolbarSpacer,
  Title,
  Label,
  Dialog,
  Bar,
  Form,
  FormItem,
} from "@ui5/webcomponents-react";
import CustomAction from "./CustomAction";
import CustomFieldItem from "../CustomFieldItem";
import getFieldValue from "../../functions/getFieldValue";
export default function TableActions(props) {
  const {
    onCreate,
    onCustomAction,
    actionParam,
    itemSelected,
    rowCount,
    property,
    suggestion,
  } = props;
  const dialogRef = useRef();
  const [currentAction, setCurrentAction] = useState({});
  const [parameterValue, setParameterValue] = useState({});
  const onCustomActionClick = function (event) {
    var customAction = actionParam.customAction[event.target.id];
    if (customAction) {
      if (customAction.parameter) {
        setCurrentAction({
          id: event.target.id,
          parameter: customAction.parameter,
          label: customAction.label,
          url: customAction.url,
        });
        dialogRef.current.show();
      } else {
        onCustomAction(event.target.id);
      }
    }
  };
  const onDialogConfirm = function () {
    dialogRef.current.close();
    onCustomAction(currentAction.id, parameterValue);
  };
  const onFieldChange = function (e) {
    const parentID = e.target.id;
    var localParameterValue = parameterValue;
    localParameterValue[parentID] = getFieldValue(e);
    setParameterValue(localParameterValue);
  };
  return (
    <>
      <Toolbar>
        <Title level="H3">{`items(${rowCount})`}</Title>
        <ToolbarSpacer />
        {/* custom actions */}
        <CustomAction
          actionParam={
            actionParam && actionParam.customAction
              ? actionParam.customAction
              : {}
          }
          onClick={onCustomActionClick}
          itemSelected={itemSelected}
        />
        {/* create action */}
        {actionParam === undefined || actionParam.CreateEnabled !== "true" ? (
          <Label hidden={true} id="emptyLabel" />
        ) : (
          <Button design={ButtonDesign.Emphasized} onClick={onCreate}>
            Create
          </Button>
        )}
      </Toolbar>
      <Dialog
        ref={dialogRef}
        draggable={true}
        resizable={true}
        initialFocus={""}
        footer={
          <Bar
            endContent={
              <>
                <Button
                  design={ButtonDesign.None}
                  onClick={function noRefCheck() {
                    dialogRef.current.close();
                  }}
                >
                  Close
                </Button>
                <Button
                  design={ButtonDesign.Emphasized}
                  onClick={onDialogConfirm}
                >
                  Confirm
                </Button>
              </>
            }
          />
        }
        headerText={currentAction.label}
      >
        <Form style={{ margin: "10px", marginInlineStart: "50px" }}>
          {currentAction.parameter &&
          Array.isArray(currentAction.parameter.value) ? (
            currentAction.parameter.value.map((param) => (
              <FormItem label={property[param] ? property[param].label : ""}>
                <CustomFieldItem
                  component={
                    property[param] ? property[param].editComponent : ""
                  }
                  field={param}
                  value=""
                  suggestion={suggestion ? suggestion[param] : {}}
                  onFilterChange={onFieldChange}
                />
              </FormItem>
            ))
          ) : (
            <></>
          )}
        </Form>
      </Dialog>
    </>
  );
}
