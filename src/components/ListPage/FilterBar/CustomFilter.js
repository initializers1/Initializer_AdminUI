import React from "react";
import { FilterBar, FilterGroupItem } from "@ui5/webcomponents-react";
import CustomSearch from "./CustomSearch";
import CustomFieldItem from "../../CustomFieldItem.js";

export default function CustomFilter(props) {
  const searchProps = props.search;
  const filterProps = props.filter === undefined ? [] : props.filter;
  const suggestion = props.suggestion;
  if (
    Object.keys(props.property).length <= 0 ||
    (props.search === undefined && props.filter === undefined)
  ) {
    return <></>;
  }
  return (
    <FilterBar
      id="FilterBar--ListPage"
      showGoOnFB
      showClearOnFB
      useToolbar={false}
      // useToolbar={window.screen.width >= 520 ? false: true} //enable toolbar for mobile view
      filterBarExpanded={false}
      onGo={props.onGoClick}
      onClear={props.onClearFilterBar}
    >
      {searchProps ? (
        <FilterGroupItem
          id={"searchBar--filterGroup"}
          style={{ marginLeft: "10px" }}
        >
          <CustomSearch
            id={"searchBar"}
            property={searchProps}
            onFilterChange={props.onFilterChange}
          />
        </FilterGroupItem>
      ) : (
        <></>
      )}
      {filterProps.map((filter) => {
        return (
          <FilterGroupItem
            id={`FilterGroupItem--${filter}`}
            key={`FilterGroupItem--${filter}`}
            label={
              props.property[filter].label === undefined
                ? filter
                : props.property[filter].label
            }
            style={{ marginLeft: "10px" }}
          >
            <CustomFieldItem
              id={`CustomFieldItem--${filter}`}
              label={
                props.property[filter].label === undefined
                  ? filter
                  : props.property[filter].label
              }
              component={props.property[filter].filterComponent}
              field={filter}
              suggestion={
                suggestion === undefined
                  ? []
                  : suggestion[props.property[filter].suggestion]
              }
              onFilterChange={props.onFilterChange}
            />
          </FilterGroupItem>
        );
      })}
    </FilterBar>
  );
}
