import React from 'react';
import ColumnWithProp from './ColumnWithProp';
import '@ui5/webcomponents/dist/TableColumn';

export default function CustomColumn(props) {
    var key = 0;
    const getProperty = (columnName) => {
        const property = props.property[columnName];
        if (property !== undefined) {
            return property;
        } else return "LOW";
    }
    return (
        <>
            {
                props.columnName.map(columnName => (
                    <ColumnWithProp key={`ColumnWithProp--${key++}`} property={getProperty(columnName)} columnName={columnName} />
                ))
            }
            <ui5-table-column slot="columns"/>
        </>
    );
}