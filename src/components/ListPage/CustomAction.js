import React from "react";
import { Button, FlexBox } from "@ui5/webcomponents-react";
export default function CustomAction(props) {
  const { onClick, actionParam, itemSelected } = props;
  return (
    <FlexBox>
      {Object.values(actionParam).map((action) => (
        <Button
          key={action.id}
          id={action.id}
          design={action.design}
          disabled={action.type === "instance" && !itemSelected.selected ? true : false}
          onClick={onClick}
        >
          {action.label}
        </Button>
        ))}
    </FlexBox>
  );
}
