import React, { useState, useEffect } from "react";

import LuigiClient, {
  addInitListener,
  addContextUpdateListener,
} from "@luigi-project/client";
import CustomTable from "./CustomTable";
import { IllustratedMessage } from "@ui5/webcomponents-react";
import CustomFilter from "./FilterBar/CustomFilter";
import "@ui5/webcomponents/dist/features/InputSuggestions.js";
import getFilterValue from "../../functions/getFilterValue";
import clearFieldsValue from "../../functions/clearFieldsValue";
import constructFilterValue from "../../functions/constructFilterValue";
import copyEntitySet from "../../functions/copyEntitySet";
import annotation from "../../annotation/annotation.json";
import Axios from "../../api/axios";
import TableActions from "./TableActions";

import "@ui5/webcomponents-fiori/dist/illustrations/NoData.js";
import "@ui5/webcomponents-fiori/dist/illustrations/UnableToLoad.js";
import "@ui5/webcomponents-fiori/dist/illustrations/NoSearchResults.js";

export default function ItemList(props) {
  const [context, setContext] = useState("");
  const [entityData, setEntityData] = useState([]);
  const [property, setProperty] = useState({});
  const [filterProps, setFilterProps] = useState([]);
  const [searchProps, setSearchProps] = useState({});
  const [suggestion, setSuggestion] = useState({});
  const [action, setAction] = useState({});
  const [listPage, setListPage] = useState([]);
  const [PageTitle, setPageTitle] = useState("");
  const [filterValue, setFilterValue] = useState({});
  const [pageDetails, setPageDetails] = useState({});
  const [errorType, setErrorType] = useState("");
  const [itemSelected, setItemSelected] = useState({
    selected: false,
    index: [],
  });
  //error messages
  const dataLoadIssueMessage = {
    text: "unable to load Data, list check your internet connection once",
    type: "error",
    closeAfter: 3000,
  };
  //load context
  useEffect(() => {
    const initListener = addInitListener((e) => {
      if (props.isDerived && props.context) {
        setContext(props.context);
      } else {
        setContext(
          LuigiClient.getContext().parentNavigationContexts === undefined
            ? ""
            : LuigiClient.getContext().parentNavigationContexts[0]
        );
      }
    });
    const updateListener = addContextUpdateListener((e) => {
      //check context is available from props
      if (props.isDerived && props.context) {
        setContext(props.context);
      } else {
        setContext(
          LuigiClient.getContext().parentNavigationContexts === undefined
            ? ""
            : LuigiClient.getContext().parentNavigationContexts[0]
        );
      }
    });
  }, []);
  //load data
  useEffect(() => {
    if (context !== undefined && context !== "") {
      LuigiClient.uxManager().showLoadingIndicator();

      if (props.isDerived && props.data) {
        setEntityData(props.data);
        setPageDetails({
          last: true,
          pageable: undefined,
          totalElements: props.data.length ? props.data.length : 0,
        });
        LuigiClient.uxManager().hideLoadingIndicator();
      } else {
        onGoClick();
      }
    }
  }, [context, props.data]);
  useEffect(() => {
    if (props.isDerived) {
      setListPage(props.listPage);
      setProperty(props.property);
      setSearchProps(props.searchProps);
      setFilterProps(props.filterProps);
      setAction(props.action);
      setItemSelected({
        selected: false,
        index: [],
      });
    } else {
      if (annotation[context] !== undefined) {
        setListPage(annotation[context].listPage);
        setProperty(annotation[context].property);
        setSearchProps(annotation[context].search);
        setFilterProps(annotation[context].filter);
        setSuggestion(annotation[context].suggestion);
        setAction(annotation[context].action);
        setPageTitle(annotation[context].title);
        setItemSelected({
          selected: false,
          index: [],
        });
      }
    }
  }, [context]);

  const onRowClick = (e) => {
    let link = "";
    if (props.context !== undefined) {
      link = LuigiClient.linkManager().withParams({
        type: props.context,
        action: "Read",
      });
    } else {
      link = LuigiClient.linkManager().withParams({
        type: LuigiClient.getContext().parentNavigationContexts[0],
        action: "Read",
      });
    }
    if(props.isDerived) {
      link.navigate(`/admin-home/objectPage/${e.detail.row.id}`);
    } else {
      link.navigate(`/admin-home/objectPage/${e.detail.row.id}`);
    }
  };

  const onGoClick = () => {
    let filter = constructFilterValue(filterValue);
    LuigiClient.uxManager().showLoadingIndicator();
    Axios.get(
      `${context}${filter}&offset=0&count=${process.env.REACT_APP_LIST_PAGE_SIZE}`
    )
      .then((response) => {
        if (response.data) {
          var data = response.data;
          if (data.content && data.content.length < 1) {
            if (filter !== "?") {
              setErrorType("NoSearchResults");
            } else {
              setErrorType("NoData");
            }
          } else {
            setErrorType("");
          }
          setEntityData(data.content);
          setPageDetails({
            last: data.last,
            pageable: data.pageable,
            totalElements: data.totalElements,
          });
          LuigiClient.uxManager().hideLoadingIndicator();
        }
      })
      .catch((err) => {
        LuigiClient.uxManager().hideLoadingIndicator();
        setErrorType("UnableToLoad");
      });
  };

  const onFilterChange = (e) => {
    const DOMparentID = e.target.id;
    let tempFilter = filterValue;
    let value = getFilterValue(e);
    if (value === "") {
      delete tempFilter[DOMparentID];
      //   event triggered by key press might have empty value
    } else {
      tempFilter[DOMparentID] = value;
    }
    setFilterValue(tempFilter);

    // on Press of Enter
    if (e.charCode === 13) {
      onGoClick();
    }
  };
  const onClearFilterBar = (e) => {
    clearFieldsValue(Object.keys(filterValue));
    setFilterValue({});
  };
  const onCreate = (e) => {
    let link = "";
    if (props.context !== undefined) {
      link = LuigiClient.linkManager().withParams({
        type: props.context,
        action: "Create",
        parentId: props.parentId,
      });
    } else {
      link = LuigiClient.linkManager().withParams({
        type: LuigiClient.getContext().parentNavigationContexts[0],
        action: "Create",
      });
    }
    link.navigate(`/admin-home/objectPage/id`);
  };
  const onCustomAction = (id, value) => {
    if (action["customAction"][id]) {
      var customAction = action["customAction"][id];
      var sideEffects = action["customAction"][id].sideEffects;
      value["id"] = itemSelected.index;
      LuigiClient.uxManager().showLoadingIndicator();
      Axios({
        method: customAction.method,
        url: `/${context}${customAction.url}`,
        data: value,
      })
        .then((response) => {
          if (response.data) {
            var data = copyEntitySet(entityData, response.data, sideEffects);
            setEntityData([...data]);
          }
          LuigiClient.uxManager().hideLoadingIndicator();
        })
        .catch((error) => {
          LuigiClient.uxManager().hideLoadingIndicator();
          LuigiClient.uxManager().showAlert(dataLoadIssueMessage);
        });
    }
  };
  const onSelectionChange = function (event) {
    var tableRef = event.target;
    var selectedRows = tableRef.selectedRows;
    if (Array.isArray(selectedRows)) {
      setItemSelected({
        selected: selectedRows.length > 0,
        index: selectedRows.map((row) => row.id),
      });
    }
  };
  if (!entityData) {
    return <></>;
  }
  return (
    <div>
      <section className="fd-section">
        {!props.isDerived ? ( //hide header title for list in object page
          <div className="fd-section__header">
            <h1 className="fd-section__title">
              {PageTitle ? PageTitle : context}
            </h1>
          </div>
        ) : (
          <></>
        )}
        <div className="header">
          {
            <CustomFilter
              search={searchProps}
              filter={filterProps}
              suggestion={suggestion}
              property={property}
              onFilterChange={onFilterChange}
              onGoClick={onGoClick}
              onClearFilterBar={onClearFilterBar}
            />
          }
        </div>
        <TableActions
          property={property}
          suggestion={suggestion}
          actionParam={action}
          itemSelected={itemSelected}
          rowCount={pageDetails.totalElements ? pageDetails.totalElements : 0}
          onCreate={onCreate}
          onCustomAction={onCustomAction}
        />
        {errorType !== undefined && errorType === "" ? (
          <CustomTable
            property={property}
            listPage={listPage}
            filterValue={filterValue}
            context={context}
            initialPageDetails={pageDetails}
            tableData={entityData}
            onRowClick={onRowClick}
            onSelectionChange={onSelectionChange}
            setEntityData={setEntityData}
            isDerived={props.isDerived}
          />
        ) : (
          <IllustratedMessage name={errorType} />
        )}
      </section>
    </div>
  );
}
