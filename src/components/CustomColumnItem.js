import React from "react";
import {
  Label,
  Avatar,
  AvatarShape,
  ObjectStatus,
  Text,
} from "@ui5/webcomponents-react";

import { WrappingType } from "@ui5/webcomponents-react";

export default function CustomColumnItem(props) {
  const value = props.value;
  switch (props.property.component) {
    case "Avatar":
      if (Array.isArray(value) && value.length > 0) {
        return (
          <Avatar shape={AvatarShape.Square} initials="i">
              <img alt={"items"} src={value[0]} style={{ objectFit: "contain" }}></img>
          </Avatar>
        );
      } else if (!Array.isArray(value) && value !== "") {
        return (
          <Avatar
            shape={AvatarShape.Square}
            children={<img alt="" src={value}></img>}
          />
        );
      } else {
        return <Avatar shape={AvatarShape.Square} initials="I" />;
      }
    case "ObjectStatus":
      var criticality = "None";
      if (props.criticality && Array.isArray(props.criticality)) {
        var criticalityObject = props.criticality.find(
          (criticality) => criticality.value === value
        );
        if (criticalityObject) {
          criticality = criticalityObject.state;
        }
      }
      return (
        <ObjectStatus state={criticality} inverted={true}>
          {" "}
          {value}
        </ObjectStatus>
      );
    case "Text":
      return <Text wrap={true}>{value}</Text>;
    default:
      return <Label wrappingType={WrappingType.Normal}>{value}</Label>;
  }
}
