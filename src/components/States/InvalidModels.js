class InvalidModels {
    constructor() {
        if(!InvalidModels._instance) {
            InvalidModels._instance = this;
            InvalidModels.models = [];
        }
        return InvalidModels._instance;
    }
    static getInstance() {
        return InvalidModels._instance;
    }
    getInvalidModels () {
        return InvalidModels.models;
    }
    setInvalidModels(model) {
        InvalidModels.models.push(model); 
    }
}
const invalidModels = new InvalidModels();
export default invalidModels;