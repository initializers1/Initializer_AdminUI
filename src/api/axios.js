import Axios from "axios";
import LuigiClient from "@luigi-project/client";

// const instance = Axios.create(`${process.env.REACT_APP_DOMAIN}/admin/`);
// const instance = Axios.create({ baseURL: `http://localhost:8080/admin/` });
// const instance = Axios.create({ baseURL: `http://192.168.43.147:8080/admin/` });
// const instance = Axios.create({baseURL: `http://localhost:5000/admin/`});
const instance = Axios.create({baseURL: `http://ec2-13-232-86-114.ap-south-1.compute.amazonaws.com:5000/admin/`});
instance.interceptors.request.use(
  (req) => {
    var accessToken = LuigiClient.getToken();
    if (!accessToken) {
      const luigiAuth = JSON.parse(localStorage.getItem("luigi.auth"));
      accessToken = luigiAuth?.accessToken;
    }
    req.headers.common["Authorization"] = "Bearer " + accessToken;
    return req;
  },
  (error) => {
    Promise.reject(error);
  }
);

export default instance;
