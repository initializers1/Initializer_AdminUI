import React, { Component } from "react";
import { render } from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";
import Home from "./views/home.js";
import DashBoardFun from "./views/dashboard-fun.js";
import ListPage from "./components/ListPage/ListPage.js";
import ObjectForm from "./components/ObjectPage/ObjectForm";
import Login from "./components/Auth/Login.js";
import Register from "./components/Auth/Register.js";
import GenerateApp from "./components/GenerateApp/GenerateApp.js";
import BuildApp from "./components/GenerateApp/BuildApp.js";
import {
  addInitListener,
  addCustomMessageListener,
  sendCustomMessage,
} from "@luigi-project/client";
import "./index.css";
import { setTheme } from "@ui5/webcomponents-base/dist/config/Theme";
import "@ui5/webcomponents/dist/Assets";
import "@ui5/webcomponents-fiori/dist/Assets";
import { ThemeProvider } from "@ui5/webcomponents-react";

class App extends Component {
  constructor(props) {
    super(props);
    addInitListener(() => {
      var theme = localStorage.getItem("initializers.user.theme");
      if (!theme) {
        const darkTheme = window.matchMedia("(prefers-color-scheme: dark)");
        if (darkTheme.matches) {
          theme = "sap_fiori_3_dark";
        } else {
          theme = "sap_fiori_3";
        }
      }
      sendCustomMessage({ id: "initializers.themechange-core", data: theme });
      setTheme(theme);
    });
    addCustomMessageListener("initializers.theme-updated", (data) => {
      setTheme(data.dataField1);
    });
  }
  render() {
    return (
      <BrowserRouter basename={`sampleapp.html#`}>
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/home" component={Home} />
        <Route path="/dashboard" component={DashBoardFun} />
        <Route path="/item-list" component={ListPage} />
        <Route path="/object-form" component={ObjectForm} />
        <Route path="/generateApp" component={GenerateApp} />
        <Route path="/buildApp" component={BuildApp} />
      </BrowserRouter>
    );
  }
}

render(
  <div>
    <ThemeProvider>
      <App />
    </ThemeProvider>
  </div>,
  document.getElementById("root")
);
