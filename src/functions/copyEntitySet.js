export default function copyEntitySet(
  originalEntities,
  updatedEntities,
  fieldsToUpdate
) {
  if (Array.isArray(originalEntities)) {
    return originalEntities.map((entity) => {
      var copyEntity = updatedEntities.find((updateEntity) => {
        if (updateEntity.id && entity.id) {
          return updateEntity.id === entity.id;
        }
        return false;
      });
      if (copyEntity) {
        var oritinalEntity = entity;
        fieldsToUpdate.forEach((field) => {
          if (oritinalEntity[field] && copyEntity[field]) {
            oritinalEntity[field] = copyEntity[field];
          }
        });
        return oritinalEntity;
      } else {
        return entity;
      }
    });
  } else {
    return originalEntities;
  }
}
