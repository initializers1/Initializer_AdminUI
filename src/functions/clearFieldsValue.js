export default function clearFieldsValue(fieldsName) {
  fieldsName.forEach((element) => {
    var fieldComponent = document.getElementById(element);
    switch (fieldComponent.nodeName) {
      case "UI5-MULTI-COMBOBOX":
        fieldComponent &&
          fieldComponent.children.forEach((child) => {
            child.selected = false;
          });
        break;
      case "UI5-COMBOBOX":
      case "UI5-INPUT":
      case "UI5-DATERANGE-PICKER":
      case "UI5-DATEPICKER":
        fieldComponent.value = "";
        break;
      default:
        break;
    }
  });
}
