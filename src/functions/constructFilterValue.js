const formatFilterValue = (filterValue, searchProps) => {
  filterValue = filterValue.toString().replaceAll(",", "~");
  filterValue = filterValue.toString().replaceAll(" ", "_");
  return filterValue;
};

export default function constructFilterValue(filterValue) {
    let filter = "?";
    let search = "";
    Object.keys(filterValue).forEach((value) => {
    // For controls with multiple input e.g date range -> first and last date
      if (Array.isArray(filterValue[value])) {
        filterValue[value].forEach((arrayValue) => {
          filter += `&filter=${value}+${
            arrayValue.operator
          }+(${formatFilterValue(arrayValue.operend)})`;
        });
      } else {
        if (filterValue[value].operend && filterValue[value].operend !== "") {
          filterValue[value].operend = formatFilterValue(
            filterValue[value].operend
          );
          if (value === "searchBar") {
              search = `&search=${filterValue[value].operend}`;
          } else {
            filter += `&filter=${value}+${filterValue[value].operator}+(${filterValue[value].operend})`;
          }
        }
      }
    });
    return `${filter}${search}`;
};
