class CustomIDPProvider {
  constructor(configSettings = {}) {
    const defaultSettings = {
      redirect_uri: window.location.origin + "/sampleapp.html#/home",
    };
    this.settings = Object.assign({}, defaultSettings, configSettings);
  }
  login() {
    return new Promise(function (resolve, reject) {
      // set auth data from local store
      Luigi.auth().store.setAuthData(Luigi.auth().store.getAuthData());
      Luigi.auth().store.setNewlyAuthorized();
      if (location.href.includes("login")) {
        Luigi.navigation().navigate("/admin-home");
      } else if (location.href.includes("register")) {
        Luigi.navigation().navigate("/admin-home/buildApp");
      }
      Luigi.configChanged(
        "settings.header",
        "settings",
        "navigation.nodes",
        "navigation.profile"
      );
      resolve();
    });
  }
  logout(authData, logoutLuigiCore) {
    const settings = {
      header: "Sign Out",
      body: "Are you sure you want Sign Out?",
      buttonConfirm: "Yes",
      buttonDismiss: "No",
    };
    Luigi.ux()
      .showConfirmationModal(settings)
      .then(() => {
        Luigi.navigation().navigate("/login");
        localStorage.removeItem("initializers.user.name");
        localStorage.removeItem("initializers.user.role");
        localStorage.removeItem("initializers.user.appName");
        localStorage.removeItem("initializers.user.appIcon");
        logoutLuigiCore();
        var updatedConfig = Luigi.getConfig();
        updatedConfig.settings.hideNavigation = true;
        Luigi.setConfig(updatedConfig);
        Luigi.configChanged("settings.header", "settings", "navigation.nodes");
      })
      .catch(() => {});
  }

  setTokenExpirationAction() {}

  setTokenExpireSoonAction() {}

  generateNonce() {
    // returns a string
  }

  userInfo() {
    // logic to get some user information
    // returns a promise of a userinfo object which contains an object with `name`, `email` and `picture` properties to display in the profile dropdown menu
    return new Promise(function (resolve, reject) {
      var userName = localStorage.getItem("initializers.user.name");
      resolve({
        name: userName ? userName : "Default User",
        email: userName,
        picture: "",
      });
    });
  }

  unload() {
    // logic that is called if you use Luigi.unload() in order to remove event listeners and intervals.
  }
}
// CustomIDPProvider.login = function() {};
