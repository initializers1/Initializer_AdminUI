self.onmessage = (event) => {
  if (event && event.data.msg === "uploadImage") {
    const filesRef = event.data.file;
    const formType = event.data.type;
    const parentId = event.data.parentId;
    const domain = event.data.domain;
    let form = new FormData();
    var newImages = [];
    form.append("type", formType);
    form.append("id", parentId);
    for (let [key, value] of Object.entries(filesRef)) {
      if (value.size > 5048576) {
        self.postMessage({
          msg: "Error",
          errorMessage: `Maximum limit for Image File exceeded, File Name: ${value.name}, Maximum File Size: 5MB`,
        });
        close();
      } else {
        newImages.push(URL.createObjectURL(value));
        form.append("images", value);
      }
    }
    fetch(`${domain}/admin/imageUpload`, {
      method: "post",
      body: form,
      headers: {
        // 'X-Tenant': 'com.initializers.fantasticfouraic7z'
        "X-Tenant": "com.initializers.groccerystore1kavf",
      },
    })
      .then((response) => {
        if (response.status === 200) {
          response.json().then((data) => {
            self.postMessage({ msg: "success", newImages: newImages });
          });
        } else {
          self.postMessage({
            msg: "Error",
            errorMessage:
              "Unable to upload image, please try again after sometime",
          });
        }
      })
      .catch((error) => {
        self.postMessage({
          msg: "Error",
          errorMessage:
            "Unable to upload image, please try again after sometime",
        });
      });
  }
};
