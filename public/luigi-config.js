var userRoles = [];
const logo =
  "https://res.cloudinary.com/dsywyhhdl/image/upload/v1590678445/Capture_tndec6.png";
const nodeAccessibilityResolver = (
  nodeToCheckPermissionFor,
  parentNode,
  currentContext
) => {
  if (parentNode.pathSegment === "admin-home") {
    if (userRoles.includes(nodeToCheckPermissionFor.access)) return true;
    if (!nodeToCheckPermissionFor.access) {
      // check if all the child has required permission
      nodeToCheckPermissionFor.children?.forEach((child) => {
        if (!userRoles.includes(child.access)) return false;
      });
      return true;
    }
    return false;
  }
  return true;
};

Luigi.setConfig({
  navigation: {
    viewGroupSettings: {
      item: {
        preloadUrl: "/sampleapp.html#/item-list",
      },
    },
    nodeAccessibilityResolver: nodeAccessibilityResolver,
    profile: {
      logout: {
        label: "Sign Out",
        icon: "sys-cancel",
        customLogoutFn: () => {
          Luigi.auth().logout();
          var updatedConfig = Luigi.getConfig();
          updatedConfig.settings.hideNavigation = true;
          Luigi.setConfig(updatedConfig);
        },
      },
    },
    nodes: () => [
      {
        pathSegment: "admin-home",
        label: "Home",
        icon: "home",
        viewUrl: "/sampleapp.html#/home",
        children: [
          {
            navigationContext: "itemDetails",
            viewGroup: "item",
            label: "Item Details",
            category: {
              label: "Item Info",
              icon: "blank-tag-2",
              collapsible: true,
            },
            // access: "READ_ITEMDETAILS",
            access: "READ_ITEM",
            pathSegment: "itemDetails",
            viewUrl: "/sampleapp.html#/item-list",
          },
          {
            viewGroup: "item",
            label: "Item Category",
            category: {
              label: "Item Info",
              icon: "blank-tag-2",
              collapsible: true,
            },
            access: "READ_ITEM",
            navigationContext: "itemCategory",
            pathSegment: "#itemCategory",
            viewUrl: "/sampleapp.html#/item-list",
          },
          {
            viewGroup: "item",
            label: "Item SubCategory",
            category: {
              label: "Item Info",
              icon: "blank-tag-2",
              collapsible: true,
            },
            access: "READ_ITEM",
            navigationContext: "itemSubcategory",
            pathSegment: "itemSubcategory",
            viewUrl: "/sampleapp.html#/item-list",
          },
          {
            viewGroup: "item",
            hideSideNav: true,
            category: {
              label: "Item Info",
              icon: "blank-tag-2",
              collapsible: true,
            },
            access: "READ_ITEM",
            navigationContext: "itemAvailability",
            pathSegment: "itemAvailability",
            viewUrl: "/sampleapp.html#/item-list",
          },
          {
            viewGroup: "item",
            label: "object view page",
            hideFromNav: true,
            hideSideNav: true,
            pathSegment: "objectPage/:id",
            access: "READ_ITEM",
            context: {
              id: ":id",
            },
            access: "READ_ITEM",
            viewUrl: "/sampleapp.html#/object-form",
          },
          {
            pathSegment: "configure-order",
            label: "Orders",
            category: {
              label: "Configure",
              icon: "provision",
              collapsible: true,
            },
            loadingIndicator: {
              enabled: false,
            },
            access: "CONFIG_ORDER",
            viewUrl: "/sampleapp.html#/item-list",
          },
          {
            pathSegment: "homePage",
            label: "Home page",
            category: {
              label: "Configure",
              icon: "provision",
              collapsible: true,
            },
            access: "CONFIG_HOMEPAGE",
            navigationContext: "homePage",
            loadingIndicator: {
              enabled: false,
            },
            viewUrl: "/sampleapp.html#/item-list",
          },
          {
            pathSegment: "adminPayementGatewayKey",
            label: "Payment",
            category: {
              label: "Configure",
              icon: "provision",
              collapsible: true,
            },
            access: "CONFIG_PAYMENT",
            navigationContext: "adminPayementGatewayKey",
            loadingIndicator: {
              enabled: false,
            },
            viewUrl: "/sampleapp.html#/item-list",
          },
          {
            pathSegment: "userOrder",
            label: "User Order",
            icon: "my-sales-order",
            navigationContext: "userOrder",
            loadingIndicator: {
              enabled: false,
            },
            access: "READ_USERORDER",
            viewUrl: "/sampleapp.html#/item-list",
          },
          {
            pathSegment: "generateApp",
            label: "Build your App",
            navigationContext: "generateApp",
            loadingIndicator: {
              enabled: false,
            },
            openNodeInModal: true,
            hideSideNav: true,
            hideFromNav: true,
            access: "READ_USERORDER",
            viewUrl: "/sampleapp.html#/generateApp",
          },
          {
            pathSegment: "buildApp",
            label: "Build your App",
            icon: "desktop-mobile",
            navigationContext: "buildApp",
            access: "READ_USERORDER",
            viewUrl: "/sampleapp.html#/buildApp",
          },
        ],
      },
      {
        pathSegment: "login",
        label: "login",
        icon: "login",
        viewUrl: "/sampleapp.html#/login",
        hideSideNav: true,
        hideFromNav: true,
        anonymousAccess: true,
      },
      {
        pathSegment: "register",
        label: "register",
        icon: "register",
        viewUrl: "/sampleapp.html#/register",
        hideSideNav: true,
        hideFromNav: true,
        anonymousAccess: true,
      },
      {
        pathSegment: "custom-action",
        viewUrl: "/sampleapp.html#/customAction",
        hideFromNav: true,
      },
      {
        label: "Switch Theme",
        pathSegment: "tog",
        onNodeActivation: () => {
          let theme = window._theme;
          if (theme === "sap_fiori_3") {
            theme = "sap_fiori_3_dark";
          } else {
            theme = "sap_fiori_3";
          }
          window._theme = theme;
          const themeUrl = `https://cdn.jsdelivr.net/npm/@sap-theming/theming-base-content@11.1.30/content/Base/baseLib/${theme}/css_variables.css`;
          const themeTag = document.querySelector("#_theme");
          if (themeTag) {
            document.head.removeChild(themeTag);
          }
          Luigi.customMessages().sendToAll({
            id: "initializers.theme-updated",
            dataField1: theme,
            moreData: "",
          });
          const link = document.createElement("link");
          link.id = "_theme";
          link.href = themeUrl;
          link.rel = "stylesheet";
          document.head.appendChild(link);
          localStorage.setItem("initializers.user.theme", theme);
          //stop navigation
          return false;
        },
      },
    ],
  },
  lifecycleHooks: {
    luigiAfterInit: () => {
      var updatedConfig = Luigi.getConfig();
      if (Luigi.auth().store.getAuthData()) {
        Luigi.auth().login();
        var roles = JSON.parse(localStorage.getItem("initializers.user.role"));
        if (roles) {
          userRoles = roles.flatMap((x) => x.authority);
        }
        var appName = localStorage.getItem("initializers.user.appName");
        if (appName) {
          updatedConfig.settings.header.title = appName;
        }
        var appIcon = localStorage.getItem("initializers.user.appIcon");
        updatedConfig.settings.header.logo = appIcon ? appIcon : logo;
        updatedConfig.settings.header.favicon = appIcon ? appIcon : logo;
        Luigi.setConfig(updatedConfig);
        Luigi.configChanged("settings");
      } else {
        updatedConfig.settings.hideNavigation = true;
        Luigi.setConfig(updatedConfig);
        Luigi.configChanged("settings");
      }
    },
  },
  communication: {
    customMessagesListeners: {
      "initializers.themechange-core": (eventData) => {
        let theme = eventData.data;
        window._theme = theme;
        const themeUrl = `https://cdn.jsdelivr.net/npm/@sap-theming/theming-base-content@11.1.30/content/Base/baseLib/${theme}/css_variables.css`;
        const themeTag = document.querySelector("#_theme");
        if (themeTag) {
          document.head.removeChild(themeTag);
        }
        const link = document.createElement("link");
        link.id = "_theme";
        link.href = themeUrl;
        link.rel = "stylesheet";
        document.head.appendChild(link);
      },
      "initializers.login": (eventData) => {
        if (eventData.data) {
          var accessToken = eventData.data.accessToken;
          var userId = eventData.data.id;
          var token = decodeJwt(accessToken);

          localStorage.setItem(
            "initializers.user.name",
            eventData.data.userName
          );
          localStorage.setItem(
            "initializers.user.role",
            JSON.stringify(token.role)
          );
          if (eventData.data.appName) {
            localStorage.setItem(
              "initializers.user.appName",
              eventData.data.appName
            );
          }
          if (eventData.data.appIcon) {
            localStorage.setItem(
              "initializers.user.appIcon",
              eventData.data.appIcon
            );
          }
          if (token.role) {
            userRoles = token.role.flatMap((x) => x.authority);
          }
          Luigi.auth().store.setAuthData({
            accessToken,
            accessTokenExpirationDate: new Date(token.exp * 1000).valueOf(),
            // accessTokenExpirationDate: Date.now() + 20000,
            scope: token.role,
            idToken: userId,
          });
          Luigi.auth().store.setNewlyAuthorized();
          const currentConfig = Luigi.getConfig();
          currentConfig.settings.header.title = eventData?.data?.appName;
          currentConfig.settings.header.logo = eventData?.data?.appIcon;
          currentConfig.settings.header.favicon = eventData?.data?.appIcon;
          Luigi.unload();
          Luigi.setConfig(currentConfig);
          Luigi.navigation().navigate("/admin-home");
        }

        var updatedConfig = Luigi.getConfig();
        updatedConfig.settings.hideNavigation = false;
        Luigi.setConfig(updatedConfig);
        Luigi.configChanged("settings", "navigation.profile");
      },
    },
  },
  auth: {
    storage: "localStorage",
    use: "idpProvider",
    idpProvider: {
      idpProvider: CustomIDPProvider,
      redirect_uri: "/logout",
    },
    disableAutoLogin: true,
    events: {
      onAuthSuccessful: (settings, authData) => {
        console.log("luigiconfing", settings, authData);
      },
      onAuthError: (settings, err) => {
        console.log("luigiconfing", settings, err);
      },
      onAuthExpired: (settings) => {
        console.log("luigiconfing", settings);
      },
      onLogout: (settings) => {
        console.log("luigiconfing", settings);
      },
      onAuthExpireSoon: (settings) => {
        console.log("luigiconfing", settings);
      },
      onAuthConfigError: (settings, err) => {
        console.log("luigiconfing", settings, err);
      },
    },
  },
  routing: {
    useHashRouting: true,
  },
  settings: {
    header: {
      title: "Initializers",
      logo,
      favicon: logo,
    },
    sideNavFooterText: "© By Initializers",
    hideNavigation: false,
    responsiveNavigation: "semiCollapsible",
    theming: {
      themes: () => [
        { id: "light", name: "Light Theme" },
        { id: "sap_fiori_3_dark", name: "Dark Theme" },
      ],
      defaultTheme: "sap_fiori_3_dark",
    },
  },
});

const decodeJwt = (token) => {
  var decodeString = decodeURIComponent(
    atob(token.split(".")[1].replace("-", "+").replace("_", "/"))
      .split("")
      .map((c) => `%${("00" + c.charCodeAt(0).toString(16)).slice(-2)}`)
      .join("")
  );
  if (decodeString) {
    return JSON.parse(decodeString);
  } else {
    return null;
  }
};
